/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TSADATADEBUGALG_H
#define TSADATADEBUGALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

#include "TsaKernel/SeedHit.h"
#include "TsaKernel/SeedStub.h"
#include "TsaKernel/SeedTrack.h"

namespace Tf {
  namespace Tsa {

    /** @class DataDebugAlg TsaDataDebugAlg.h
     *
     *  Low level debug algorithm for Tsa data objects
     *
     *  @author Chris Jones
     *  @date   2007-07-17
     */

    class DataDebugAlg : public GaudiHistoAlg {

    public:
      /// Standard constructor
      DataDebugAlg( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~DataDebugAlg(); ///< Destructor

      StatusCode initialize() override; ///< Algorithm initialization
      StatusCode execute() override;    ///< Algorithm execution
      StatusCode finalize() override;   ///< Algorithm finalization

    private: // methods
      /// Debug the Seed Hits
      StatusCode debugSeedHits();

    private:                         // data
      std::string m_seedHitLocation; ///< Location in TES for Seed Hits
    };

  } // namespace Tsa
} // namespace Tf

#endif // TSADATADEBUGALG_H
