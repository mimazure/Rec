/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TsaITXSearch_H
#define _TsaITXSearch_H

#include <string>

#include "TfTsHitNumMap.h"
#include "TsaKernel/TsaSeedingHit.h"
#include "TsaKernel/TsaTStationHitManager.h"
#include "TsaXSearchBase.h"

namespace Tf {
  namespace Tsa {

    /** @class ITXSearch TsaITXSearch.h
     * Follow track and pick up hits
     * @author M. Needham
     **/
    class ITXSearch : public XSearchBase {

    public:
      /// constructer
      ITXSearch( const std::string& type, const std::string& name, const IInterface* parent );

      // destructer
      virtual ~ITXSearch();

      /// initialize
      StatusCode initialize() override;

      using Tf::Tsa::XSearchBase::execute;
      // execute method
      StatusCode execute( std::vector<SeedTrack*>& seeds, std::vector<SeedHit*> hits[6] = 0 ) override;

    private:
      void loadData( std::vector<SeedHit*> hits[6] ) const;

      std::string m_dataSvcType;
      std::string m_dataSvcName;

      typedef Tf::Tsa::TStationHitManager ITHitMan;

      /// Pointer to the data manager
      ITHitMan* m_hitMan;

      TfTsHitNumMap m_hitNumMap;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaITXSearchBase_H
