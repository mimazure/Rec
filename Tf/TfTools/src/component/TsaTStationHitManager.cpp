/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TsaTStationHitManager.cpp
 *
 *  Implemetnation file for class : Tf::Tsa::TStationHitManager
 *
 *  @author S. Hansmann-Menzemer, W. Houlsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------

// local
#include "TsaKernel/TsaTStationHitManager.h"

//-----------------------------------------------------------------------------

using namespace Tf::Tsa;

// Declaration of the Tool Factory
DECLARE_COMPONENT( TStationHitManager )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Tf::Tsa::TStationHitManager::TStationHitManager( const std::string& type, const std::string& name,
                                                 const IInterface* parent )
    : Tf::TStationHitManager<Tf::Tsa::SeedingHit>( type, name, parent ) {
  declareInterface<Tf::Tsa::TStationHitManager>( this );
}

//=============================================================================

// Not sure why this helps here, but it does suppress the warning!
#ifdef __INTEL_COMPILER            // Disable ICC warning
#  pragma warning( disable : 279 ) // BOOST_ASSERT controlling expression is constant
#endif
