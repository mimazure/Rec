/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IRegistry.h"

#include "TfKernel/DefaultVeloRHitManager.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DefaultVeloRHitManager
//
// 2007-07-04 : Kurt Rinnert <kurt.rinnert@cern.ch>
//-----------------------------------------------------------------------------
namespace Tf {

  DECLARE_COMPONENT( DefaultVeloRHitManager )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  DefaultVeloRHitManager::DefaultVeloRHitManager( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
      : DefaultVeloHitManager<DeVeloRType, VeloRHit, 4>( type, name, parent ) {
    declareInterface<DefaultVeloRHitManager>( this );
  }

} // namespace Tf
