###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackSys
################################################################################
gaudi_subdir(TrackSys v9r14)

gaudi_depends_on_subdirs(Event/TrackEvent
                         Tf/FastVelo
                         Tf/PatAlgorithms
                         Tf/PatVelo
                         Tf/PatVeloTT
                         Tf/TsaAlgorithms
                         Tf/TsaKernel
                         Tr/TrackAssociators
                         Tr/TrackCheckers
                         Tr/TrackExtrapolators
                         Tr/TrackFitEvent
                         Tr/TrackFitter
                         Tr/TrackIdealPR
                         Tr/TrackInterfaces
                         Tr/TrackMCTools
                         Tr/TrackMonitors
                         Tr/TrackProjectors
                         Tr/TrackTools
                         Tr/TrackUtils
			 DAQ/DAQSys)

gaudi_install_python_modules()

