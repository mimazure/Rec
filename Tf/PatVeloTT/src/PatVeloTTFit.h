/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_PATVELOTTFIT_H
#define INCLUDE_PATVELOTTFIT_H 1

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "PatVeloTTTool.h"
#include "TrackInterfaces/IPatVeloTTFit.h"

// forward declarations
class PatVeloTool;

/** @class PatVeloTTFit PatVeloTTFit.h
 *
 * provide a convenient interface to the internal fit used in the PatVeloTT
 * algorithm in the pattern reco
 *
 * @author Pavel Krokovny <krokovny@physi.uni-heidelberg.de>
 * @date   2009-03-10
 */
class PatVeloTTFit : public extends<GaudiTool, IPatVeloTTFit> {
public:
  /// Standard Constructor
  PatVeloTTFit( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Tool initialization

  StatusCode fitVTT( LHCb::Track& track ) const override;

  void finalFit( const std::vector<PatTTHit*>& /*theHits*/, const std::array<float, 8>& /*vars*/,
                 std::array<float, 3>& /*params*/ ) const override {
    ;
  };

private:
  PatVeloTTTool*   m_patVTTTool      = nullptr;
  DeSTDetector*    m_ttDet           = nullptr;
  PatTTMagnetTool* m_PatTTMagnetTool = nullptr; ///< Multipupose tool for Bdl and deflection
  AnyDataHandle<LHCb::STLiteCluster::FastContainer> m_TTClusters{LHCb::STLiteClusterLocation::TTClusters,
                                                                 Gaudi::DataHandle::Reader, this};
};
#endif // INCLUDE_PATVELOTTFIT_H
