/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMPONENT_MUONDETPOSTOOL_H
#define COMPONENT_MUONDETPOSTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/IMuonFastPosTool.h" // Interface
class DeMuonDetector;

/** @class MuonDetPosTool MuonDetPosTool.h component/MuonDetPosTool.h
 *
 *
 *  @author Giacomo GRAZIANI
 *  @date   2009-03-17
 */
class MuonDetPosTool : public extends<GaudiTool, IMuonFastPosTool> {
public:
  /// Standard constructor
  MuonDetPosTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~MuonDetPosTool() override; ///< Destructor
  StatusCode initialize() override;
  StatusCode calcTilePos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay, double& z,
                          double& deltaz ) const override;

  StatusCode calcStripXPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override;

  StatusCode calcStripYPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override;

private:
  DeMuonDetector* m_muonDetector = nullptr;
};
#endif // COMPONENT_MUONDETPOSTOOL_H
