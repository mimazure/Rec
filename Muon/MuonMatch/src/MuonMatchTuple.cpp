/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <functional>
#include <utility>

// range
#include <range/v3/view/transform.hpp>

// Local
#include "MuonMatchTuple.h"

using namespace MuonMatch;

namespace {
  using TrackToMCParticlesLinker = InputLinks<tmp::Track, LHCb::MCParticle>;
  using MCParticleToMCHitLinker  = LinkedFrom<LHCb::MCHit, LHCb::MCParticle>;
} // namespace

static const float InvalidPosMagnitude = -100.;
static const int   InvalidMCKey        = -1;
static const int   NoMatchedMCParticle = 0;
static const int   NullCharge          = 0;
static const float NullWeight          = 0.;

DECLARE_COMPONENT( MuonMatchHitsTuple )
DECLARE_COMPONENT( MuonMatchTracksTuple )

//=============================================================================
//
StatusCode MuonMatchTupleAlg::initialize() {

  // Initialize magnetic field service
  m_fieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  return StatusCode::SUCCESS;
}

//=============================================================================
//
void MuonMatchTupleAlg::fill_with_event_info( Tuples::Tuple& tuple, const LHCb::ODIN& odin ) const {

  tuple->column( "RunNumber", odin.runNumber() );
  tuple->column( "EvtNumber", odin.eventNumber() );
  tuple->column( "Polarity", m_fieldSvc->isDown() ? -1 : +1 );
}

//=============================================================================
//
void MuonMatchTupleAlg::fill_with_mcmatch( Tuples::Tuple& tuple, const std::string& prefix,
                                           const LHCb::MCParticle* particle ) const {

  tuple->column( prefix + "_KEY", particle ? particle->key() : InvalidMCKey );
  tuple->column( prefix + "_PID", particle ? particle->particleID().pid() : NoMatchedMCParticle );
  tuple->column( prefix + "_P", particle ? particle->p() : InvalidPosMagnitude );
  tuple->column( prefix + "_PT", particle ? particle->pt() : InvalidPosMagnitude );
  tuple->column( prefix + "_ETA", particle ? particle->pseudoRapidity() : InvalidPosMagnitude );
  tuple->column( prefix + "_Q", particle ? particle->particleID().threeCharge() / 3 : NullCharge );
}

//=============================================================================
//
MuonMatchTracksTuple::MuonMatchTracksTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputTracks", LHCb::TrackLocation::Forward},
                 KeyValue{"InputVertices", LHCb::Event::v2::RecVertexLocation::Velo3D},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"LinkerLocation", Links::location( LHCb::TrackLocation::Forward )}} ) {}

//=============================================================================
//
void MuonMatchTracksTuple::operator()( const tmp::Tracks& tracks, const Vertices& pvs, const LHCb::ODIN& odin,
                                       const LHCb::LinksByKey& links ) const {

  // N-Tuple to fill
  Tuples::Tuple tuple = nTuple( "Tracks", "Tracks", CLID_ColumnWiseTuple );

  // Access the linker table
  const TrackToMCParticlesLinker linker( links );

  // To calculate the IP of the track
  auto ip_calculator = [&pvs]( const tmp::Track& t ) -> float {
    if ( !pvs.size() ) return InvalidPosMagnitude;

    const auto ips2 =
        ranges::v3::view::transform( pvs, [&t]( const Vertex& pv ) { return squared_impact_parameter( t, pv ); } );

    return std::sqrt( *std::min_element( ips2.begin(), ips2.end() ) );
  };

  for ( const auto track : tracks ) {

    // Fill with general magnitudes
    tuple->column( "P", track->p() );
    tuple->column( "PT", track->pt() );
    tuple->column( "ETA", track->pseudoRapidity() );
    tuple->column( "Q", track->charge() );
    tuple->column( "PHI", track->phi() );
    tuple->column( "IP", ip_calculator( *track ) );

    // Fill track content
    tuple->column( "HAS_VELO", track->hasVelo() );
    tuple->column( "HAS_UT", track->hasUT() );
    tuple->column( "HAS_T", track->hasT() );

    // Fill with MC-matching information
    const auto relations = linker.from( track->key() );

    std::vector<std::pair<LHCb::MCParticle*, float>> mcmatch_info;

    for ( const auto rel : relations ) {

      const auto mcp = rel.to();

      if ( !mcp ) continue;

      mcmatch_info.emplace_back( mcp, rel.weight() );
    }

    const auto best = std::max_element( mcmatch_info.cbegin(), mcmatch_info.cend(), []( const auto& f, const auto& s ) {
      const auto& f_wgt = f.second;
      const auto& s_wgt = s.second;

      return s_wgt > f_wgt ? s_wgt : f_wgt;
    } );

    const bool valid = best != mcmatch_info.cend();

    LHCb::MCParticle* part = valid ? best->first : nullptr;

    tuple->column( "MCMATCH_WGT", valid ? best->second : NullWeight );

    fill_with_mcmatch( tuple, "MCMATCH", part );

    const auto mother = valid ? part->mother() : nullptr;

    fill_with_mcmatch( tuple, "MCMATCH_MOTHER", mother );

    // Fill with state values
    for ( const auto loc : m_trackStates ) {

      const auto state = track->stateAt( static_cast<LHCb::State::Location>( loc ) );

      if ( !state ) {
        Warning( "Track from \"" + inputLocation<0>() + "\" does not have state at \"" +
                 LHCb::State::LocationToString( loc ) + "\"" )
            .ignore();
        continue;
      }

      const auto prefix = LHCb::State::LocationToString( loc ) + "_";

      tuple->column( prefix + "X", state->x() );
      tuple->column( prefix + "Y", state->y() );
      tuple->column( prefix + "Z", state->z() );
      tuple->column( prefix + "ERR_X2", state->errX2() );
      tuple->column( prefix + "ERR_Y2", state->errY2() );
      tuple->column( prefix + "TX", state->tx() );
      tuple->column( prefix + "TY", state->ty() );
      tuple->column( prefix + "ERR_TX2", state->errTx2() );
      tuple->column( prefix + "ERR_TY2", state->errTy2() );
      tuple->column( prefix + "P", state->p() );
      tuple->column( prefix + "PT", state->pt() );
      tuple->column( prefix + "QOVERP", state->qOverP() );
    }

    // Fill event info for each muon hit
    fill_with_event_info( tuple, odin );

    // Write the tuple
    tuple->write();
  }
}

//=============================================================================
//
MuonMatchHitsTuple::MuonMatchHitsTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputMuonHits", MuonHitContainerLocation::Default},
                 KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}} ) {}

//=============================================================================
//
void MuonMatchHitsTuple::operator()( const MuonHitContainer& hit_cont, const LHCb::ODIN& odin,
                                     const LHCb::MCParticles& mcparticles ) const {

  // N-Tuple to fill
  Tuples::Tuple tuple = nTuple( "Hits", "Hits", CLID_ColumnWiseTuple );

  // Access the linker table
  MCParticleToMCHitLinker linker( eventSvc(), msgSvc(), m_linkerLocation );

  // Iterate over stations
  for ( auto is = 0; is < 4; ++is ) {

    const auto& station = hit_cont.station( is );

    // Iterate over regions
    for ( unsigned int ir = 0; ir < station.nRegions(); ++ir ) {

      // Get all hits
      const auto rhits =
          hit_cont.hits( std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max(), is, ir );

      // Iterate over hits and fill N-Tuple
      for ( const auto& hit : rhits ) {

        tuple->column( "X", hit.x() );
        tuple->column( "Y", hit.y() );
        tuple->column( "Z", hit.z() );
        tuple->column( "ERR_X2", hit.dx() * hit.dx() / 12. );
        tuple->column( "ERR_Y2", hit.dy() * hit.dy() / 12. );
        tuple->column( "ERR_Z2", hit.dz() * hit.dz() / 12. );
        tuple->column( "STATION", hit.station() );
        tuple->column( "REGION", hit.region() );
        tuple->column( "UNCROSSED", hit.uncrossed() );

        // Fill event info for each muon hit
        fill_with_event_info( tuple, odin );

        // Fill MC-match information for each muon hit
        float             dist2 = std::numeric_limits<float>::max();
        LHCb::MCParticle* best  = nullptr;
        for ( auto p : mcparticles ) {

          auto mchit = linker.first( p );

          while ( mchit ) {

            const auto dx = mchit->entry().X() - hit.x();
            const auto dy = mchit->entry().Y() - hit.y();
            const auto dz = mchit->entry().Z() - hit.z();

            const auto d2 = dx * dx + dy * dy + dz * dz;

            // Must pass the threshold
            if ( d2 < dist2 ) {
              dist2 = d2;
              best  = p;
            }

            mchit = linker.next();
          }
        }

        tuple->column( "MCMATCH_SQUARED_DISTANCE", dist2 );

        fill_with_mcmatch( tuple, "MCMATCH", best );

        const auto mother = best ? best->mother() : nullptr;

        fill_with_mcmatch( tuple, "MCMATCH_MOTHER", mother );

        // Write the tuple
        tuple->write();
      }
    }
  }
}
