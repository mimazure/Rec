/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONID_IMUONMATCHTOOL_H
#define MUONID_IMUONMATCHTOOL_H 1
#include <tuple>
#include <vector>

#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"
#include "MuonDAQ/CommonMuonHit.h"

namespace LHCb {
  class MuonTileID;
}

/** @class IMuonMatchTool IMuonMatchTool.h MuonMatch/IMuonMatchTool.h
 *
 *  tool to match tracks to the muon detector, starting from pre-selected matching tables
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 */

/// TrackMuMatch contains MuonCommonHit, sigma_match, track extrap X, track extrap Y
typedef std::tuple<const CommonMuonHit*, float, float, float> TrackMuMatch;

struct IMuonMatchTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonMatchTool, 3, 0 );

  enum MuonMatchType { NoMatch = 0, Uncrossed = 1, Good = 2 };

  virtual StatusCode                    run( const LHCb::Track*, std::vector<TrackMuMatch>* bestMatches,
                                             std::vector<TrackMuMatch>* spareMatches = nullptr )                = 0;
  virtual CommonConstMuonHits           getListofCommonMuonHits( int station = -1, bool onlybest = true ) const = 0;
  virtual std::vector<LHCb::MuonTileID> getListofMuonTiles( int station = -1, bool onlybest = true ) const      = 0;
  virtual std::pair<float, int>         getChisquare() const                                                    = 0;
  virtual float                         getMaxChi2Contrib() const                                               = 0;
  virtual MuonMatchType                 getMatch( int station ) const                                           = 0;
  virtual float                         getMatchSigma( unsigned int station ) const                             = 0;
};
#endif // MUONID_IMUONMATCHTOOL_H
