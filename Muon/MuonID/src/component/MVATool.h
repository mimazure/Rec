/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MVATOOL_H
#define MVATOOL_H 1

#include <array>
#include <limits>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"

#include "MuonID/IMVATool.h" // Interface

#include "MuonDet/DeMuonDetector.h"
#include "MuonID/ICommonMuonTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "evaluator.h"
#include "model_calcer_wrapper.h"
#include "wrapped_calcer.h"

#include "weights/TMVAClassification_BDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS.class.C"

/** @class MVATool MVATool.h
 * A tool that provides an MVA for muon identification
 *
 * @author Ricardo Vazquez Gomez
 * @date 2017-05-29
 */

class MVATool final : public extends1<GaudiTool, IMVATool> {
public:
  MVATool( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~MVATool() override = default;
  virtual auto initialize() -> StatusCode override;
  virtual auto calcBDT( const LHCb::Track&, const CommonConstMuonHits& ) const noexcept -> MVAResponses override;
  // TODO(kazeevn) this probably can be replaced with
  // Gaudi::Utils::getProperty
  // 1. Can someone please show me how to get the actual bool value?
  // 2. Will this be fast enough to call on every track?
  virtual bool   getRunTMVA() const noexcept override { return m_runTMVA; }
  virtual bool   getRunCatBoostBatch() const noexcept override { return m_runCatBoostBatch; }
  virtual size_t getNFeatures() const noexcept override { return nFeatures; }
  unsigned int   nStations;
  // A way to have a nice continuous C array with data
  // result must be preallocated with nFeatures size
  virtual void                compute_features( const LHCb::Track& muTrack, const CommonConstMuonHits& hits,
                                                LHCb::span<float> result ) const noexcept override;
  virtual std::vector<double> calcBDTBatch( const std::vector<float> features ) const noexcept override;
  virtual double              calcTMVAonFeatures( LHCb::span<float> features ) const noexcept override;

private:
  // There are two libaries that allow for running CatBoost models
  // standalone_evaluator and model_interface
  // As of LCG v94 the standalone_evaluator is slightly faster when running on
  // single predictions, but model_interface supports batches
  // Logic:
  // 1. If both RunCatBoostBatch and UseCatboostStandaloneEvaluator are true, fail
  // 2. If RunCatBoostBatch is true, run in batch mode
  // 3. If RunCatBoostBatch is false and UseCatboostStandaloneEvaluator is true,
  //    run standalone_evaluator
  // 4. If RunCatBoostBatch is false and UseCatboostStandaloneEvaluator is false,
  //    run model_interface
  Gaudi::Property<bool>        m_runCatBoostBatch{this, "RunCatBoostBatch", true};
  Gaudi::Property<bool>        m_useStandAloneCatboost{this, "UseCatboostStandaloneEvaluator", false};
  Gaudi::Property<std::string> m_CatBoostModelPath{
      this, "CatBoostModelPath", System::getEnv( "TMVAWEIGHTSROOT" ) + "/data/MuID/MuID-Run2-MC-570-v1.cb"};
  Gaudi::Property<bool> m_runTMVA{this, "RunTMVA", true};
  std::unique_ptr<ReadBDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS> m_TMVA_BDT;
  ModelCalcerWrapper                                                                     m_CatBoostModel;
  // Catboost without the C++ wrapper to call predict on float**
  using CalcerHolderType = std::unique_ptr<ModelCalcerHandle, std::function<void( ModelCalcerHandle* )>>;
  CalcerHolderType                                       m_rawCatBoostModel;
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModel;
  DeMuonDetector*                                        m_det;
  ICommonMuonTool*                                       muonTool_;
  std::vector<float>                                     m_stationZ;
  unsigned int                                           firstUsedStation;
  // Features the MVA expects
  static constexpr size_t n_features_per_station = 5;
  static constexpr size_t used_stations          = 4;
  static constexpr size_t nFeatures              = n_features_per_station * used_stations;
  static constexpr size_t dt_index               = 0 * used_stations;
  static constexpr size_t time_index             = 1 * used_stations;
  static constexpr size_t crossed_index          = 2 * used_stations;
  static constexpr size_t resX_index             = 3 * used_stations;
  static constexpr size_t resY_index             = 4 * used_stations;
  // Values to use if there is no matched hit in a station
  static constexpr float                                     defaultRes       = -10000.;
  static constexpr std::array<float, n_features_per_station> default_features = {-10000., -10000., 0., defaultRes,
                                                                                 defaultRes};
};
#endif // MVATOOL_H
