/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Predict.h"
#include "Update.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Scalar {

      /**
       * @brief      Fits an initial node, whose covariance is passed by parameter
       */
      template <class T>
      inline void fit( Node& node, const std::array<TRACKVECTORFIT_PRECISION, 15>& covariance ) {
        initialize<T>( node, covariance );
        update<T>( node );
      }

      /**
       * @brief      Fits a node
       */
      template <class T>
      inline void fit( Node& node, const Node& prevnode ) {
        predict<T>( node, prevnode );
        update<T>( node );
      }

    } // namespace Scalar

  } // namespace TrackVectorFit

} // namespace Tr
