/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/State.h"
#include "Event/TrackParameters.h"
#include "Event/TrackTypes.h"
#include "GaudiKernel/VectorMap.h"
#include "Kernel/meta_enum.h"
#include <algorithm>
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations
  class Measurement;

  /** @class Node Node.h
   *
   * Node is a base class for classes linking track states to measurements.
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Tue Jan 29 11:47:53 2019
   *
   */
  namespace Enum::Node {
    meta_enum_class( Type, unsigned char, Unknown, HitOnTrack, Outlier, Reference )
  }

  class Node {
  public:
    /// enumerator for the type of Node
    using Type = Enum::Node::Type;

    /// Default Constructor
    Node() = default;

    /// Constructor from a Measurement
    Node( const LHCb::Measurement* meas );

    /// Constructor from a z-position
    Node( double z );

    /// Constructor from a z-position and a location
    Node( double z, const LHCb::State::Location& location );

    /// destructor
    virtual ~Node() = default;

    /// Clone the Node
    virtual Node* clone() const;

    /// Retrieve the local chi^2
    virtual double chi2() const;

    /// Retrieve the state
    virtual const LHCb::State& state() const { return m_state; }

    /// Retrieve the residual
    virtual double residual() const { return m_residual; }

    /// Retrieve the error in the residual
    virtual double errResidual() const { return m_errResidual; }

    /// conversion of string to enum for type Type
    static LHCb::Node::Type TypeToType( const std::string& aName );

    /// conversion to string for enum type Type
    static const std::string& TypeToString( int aEnum );

    /// Update the state vector
    void setState( const Gaudi::TrackVector& stateVector, const Gaudi::TrackSymMatrix& stateCovariance,
                   const double& z );

    /// Update the reference vector
    void setRefVector( const Gaudi::TrackVector& refVector );

    /// Update the reference vector
    void setRefVector( const LHCb::StateVector& refVector );

    /// Retrieve unbiased residual
    double unbiasedResidual() const;

    /// Retrieve error on unbiased residual
    double errUnbiasedResidual() const;

    /// Retrieve const  the reference to the measurement
    const LHCb::Measurement& measurement() const { return *m_measurement; }

    /// Set the measurement
    void setMeasurement( const LHCb::Measurement& meas ) { m_measurement = &meas; }

    /// Return the error on the residual squared
    double errResidual2() const;

    /// Return the measure error squared
    double errMeasure2() const { return m_errMeasure * m_errMeasure; }

    /// Return true if this Node has a valid pointer to measurement
    bool hasMeasurement() const { return m_measurement != nullptr; }

    /// Remove measurement from the node
    void removeMeasurement();

    /// Retrieve the unbiased smoothed state at this position
    State unbiasedState() const;

    /// z position of Node
    double z() const { return m_refVector.z(); }

    /// Position of the State on the Node
    Gaudi::XYZPoint position() const { return m_state.position(); }

    /// Set the location of the state in this node.
    void setLocation( LHCb::State::Location& location ) { m_state.setLocation( location ); }

    /// Retrieve const  type of node
    const Type& type() const { return m_type; }

    /// Update  type of node
    void setType( const Type& value ) { m_type = value; }

    /// Update  state
    void setState( const LHCb::State& value ) { m_state = value; }

    /// Retrieve const  the reference vector
    const LHCb::StateVector& refVector() const { return m_refVector; }

    /// Retrieve const  flag for the reference vector
    bool refIsSet() const { return m_refIsSet; }

    /// Update  the residual value
    void setResidual( double value ) { m_residual = value; }

    /// Update  the residual error
    void setErrResidual( double value ) { m_errResidual = value; }

    /// Retrieve const  the measure error
    double errMeasure() const { return m_errMeasure; }

    /// Update  the measure error
    void setErrMeasure( double value ) { m_errMeasure = value; }

    /// Retrieve const  the projection matrix
    const Gaudi::TrackProjectionMatrix& projectionMatrix() const { return m_projectionMatrix; }

    /// Update  the projection matrix
    void setProjectionMatrix( const Gaudi::TrackProjectionMatrix& value ) { m_projectionMatrix = value; }

    /// Retrieve const  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    double doca() const { return m_doca; }

    /// Update  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    void setDoca( double value ) { m_doca = value; }

    /// Retrieve const  Unit vector perpendicular to state and measurement
    const Gaudi::XYZVector& pocaVector() const { return m_pocaVector; }

    /// Update  Unit vector perpendicular to state and measurement
    void setPocaVector( const Gaudi::XYZVector& value ) { m_pocaVector = value; }

  protected:
    // private:

    Type                         m_type;                  ///< type of node
    LHCb::State                  m_state;                 ///< state
    LHCb::StateVector            m_refVector;             ///< the reference vector
    bool                         m_refIsSet    = false;   ///< flag for the reference vector
    const LHCb::Measurement*     m_measurement = nullptr; ///< pointer to the measurement (not owner)
    double                       m_residual    = 0;       ///< the residual value
    double                       m_errResidual = 0;       ///< the residual error
    double                       m_errMeasure  = 0;       ///< the measure error
    Gaudi::TrackProjectionMatrix m_projectionMatrix;      ///< the projection matrix
    double           m_doca = 0;   ///< Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    Gaudi::XYZVector m_pocaVector; ///< Unit vector perpendicular to state and measurement

  }; // class Node

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::Node::Node( double z ) : m_type( Type::Reference ) { m_refVector.setZ( z ); }

inline LHCb::Node::Node( double z, const LHCb::State::Location& location )
    : m_type( Type::Reference ), m_state( location ) {
  m_refVector.setZ( z );
}

inline void LHCb::Node::setState( const Gaudi::TrackVector& stateVector, const Gaudi::TrackSymMatrix& stateCovariance,
                                  const double& z ) {
  m_state.setState( stateVector );
  m_state.setCovariance( stateCovariance );
  m_state.setZ( z );
}

inline void LHCb::Node::setRefVector( const Gaudi::TrackVector& refVector ) {
  m_refIsSet               = true;
  m_refVector.parameters() = refVector;
}

inline void LHCb::Node::setRefVector( const LHCb::StateVector& refVector ) {
  m_refIsSet  = true;
  m_refVector = refVector;
}

inline double LHCb::Node::unbiasedResidual() const {
  double r = residual();
  return type() == Type::HitOnTrack ? ( r * errMeasure2() / ( m_errResidual * m_errResidual ) ) : r;
}

inline double LHCb::Node::errUnbiasedResidual() const {
  double errRes = errResidual();
  return type() == Type::HitOnTrack ? ( errMeasure2() / errRes ) : errRes;
}

inline double LHCb::Node::errResidual2() const {
  double x = errResidual();
  return x * x;
}

inline void LHCb::Node::removeMeasurement() {
  m_measurement = nullptr;
  // for a node without measurement the chi2 = 0 ;-)
  m_residual    = 0.;
  m_errResidual = 0.;
  m_errMeasure  = 0.;
}

inline LHCb::State LHCb::Node::unbiasedState() const {
  // we can redo this routine by smoothing the unfiltered states

  // This performs an inverse kalman filter step.
  // First calculate the gain matrix
  const auto& H       = projectionMatrix();
  const auto& biasedC = state().covariance();
  double      r       = residual();
  double      R       = errResidual2();

  ROOT::Math::SMatrix<double, 5, 1> K = ( biasedC * Transpose( H ) ) / R;

  // Forwarddate the state vectors
  Gaudi::TrackVector unbiasedX = state().stateVector() - K.Col( 0 ) * r;

  // Forwarddate the covariance matrix
  static const Gaudi::TrackSymMatrix unit = ROOT::Math::SMatrixIdentity();
  Gaudi::TrackSymMatrix              unbiasedC;
  ROOT::Math::AssignSym::Evaluate( unbiasedC, ( unit + K * H ) * biasedC );
  return LHCb::State{unbiasedX, unbiasedC, z(), state().location()};
}
