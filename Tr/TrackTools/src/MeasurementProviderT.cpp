/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderT MeasurementProviderT.cpp
 *
 * Implementation of templated MeasurementProvider tool
 * see interface header for description
 *
 *  @author W. Hulsbergen
 *  @date   07/06/2007
 */

#include "Event/Measurement.h"
#include "Event/StateVector.h"
#include "Event/TrackParameters.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackKernel/ZTrajectory.h"
#include <type_traits>

template <typename T>
class MeasurementProviderT final : public extends<GaudiTool, IMeasurementProvider> {
public:
  using extends::extends;

  StatusCode initialize() override;
  void       addToMeasurements( LHCb::span<LHCb::LHCbID> ids, std::vector<LHCb::Measurement>& measurements,
                                LHCb::ZTrajectory<double> const& reftraj ) const override;

  StatusCode load( LHCb::Track& ) const override {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg;
    return StatusCode::FAILURE;
  }

private:
  LHCb::Measurement measurement( typename T::Cluster const& cluster, bool localY ) const;
  LHCb::Measurement measurement( typename T::Cluster const& cluster, LHCb::ZTrajectory<double> const& refvector,
                                 bool localY ) const;

  DataObjectReadHandle<typename T::ClusterContainerType> m_clustersDH{this, "ClusterLocation",
                                                                      T::defaultClusterLocation()};

  Gaudi::Property<bool>                    m_useReference{this, "UseReference", true};
  ToolHandle<typename T::PositionToolType> m_positiontool = {T::positionToolName()};
  const typename T::DetectorType*          m_det          = nullptr;
};

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

template <typename T>
StatusCode MeasurementProviderT<T>::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<typename T::DetectorType>( T::defaultDetectorLocation() );

  return sc;
}

////////////////////////////////////////////////////////////////////////////////////////
// Template instantiations using Traits classes
////////////////////////////////////////////////////////////////////////////////////////

#include "TrackInterfaces/ISTClusterPosition.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "TrackInterfaces/IVPClusterPosition.h"
#include "TrackInterfaces/IVeloClusterPosition.h"

#include "Kernel/LineTraj.h"

#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Event/VPCluster.h"
#include "Event/VPLightCluster.h"
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "PrKernel/UTHitHandler.h"

#include "STDet/DeSTDetector.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"
#include "VPDet/DeVP.h"
#include "VeloDet/DeVelo.h"

namespace MeasurementProviderTypes {

  //////////// Velo
  struct VeloBase {
    using PositionToolType = IVeloClusterPosition;
    using DetectorType     = DeVelo;
    static std::string         positionToolName() { return "VeloClusterPosition"; }
    static std::string const&  defaultDetectorLocation() { return DeVeloLocation::Default; }
    static LHCb::VeloChannelID channelId( LHCb::LHCbID id ) { return id.veloID(); }
  };

  struct VeloPhiBase : VeloBase {
    static bool   checkType( LHCb::LHCbID id ) { return id.isVelo() && id.veloID().isPhiType(); }
    static double nominalZ( DeVelo const& det, LHCb::LHCbID id ) { return det.phiSensor( id.veloID() )->z(); }
  };
  struct VeloRBase : VeloBase {
    static bool   checkType( LHCb::LHCbID id ) { return id.isVelo() && id.veloID().isRType(); }
    static double nominalZ( DeVelo const& det, LHCb::LHCbID id ) { return det.rSensor( id.veloID() )->z(); }
  };

  struct VeloLiteBase {
    using Cluster              = LHCb::VeloLiteCluster;
    using ClusterContainerType = LHCb::VeloLiteCluster::VeloLiteClusters;
    static std::string const& defaultClusterLocation() { return LHCb::VeloLiteClusterLocation::Default; }
  };
  struct VeloFullBase {
    using Cluster              = LHCb::VeloCluster;
    using ClusterContainerType = LHCb::VeloClusters;
    static std::string const& defaultClusterLocation() { return LHCb::VeloClusterLocation::Default; }
  };

  struct VeloR : VeloFullBase, VeloRBase {
    static LHCb::Measurement makeMeasurement( LHCb::VeloCluster const& clus, DeVelo const& det,
                                              IVeloClusterPosition::toolInfo const& info ) {
      DeVeloRType const* rDet = det.rSensor( clus.channelID().sensor() );
      return {rDet->z(), rDet->trajectory( info.strip, info.fractionalPosition ),
              rDet->rPitch( info.strip.strip() ) * info.fractionalError, rDet, &clus};
    }
  };
  struct VeloPhi : VeloFullBase, VeloPhiBase {
    static LHCb::Measurement makeMeasurement( LHCb::VeloCluster const& clus, DeVelo const& det,
                                              IVeloClusterPosition::toolInfo const& info ) {
      DeVeloPhiType const* phiDet = det.phiSensor( clus.channelID() );
      return {phiDet->z(), phiDet->trajectory( info.strip, info.fractionalPosition ),
              phiDet->phiPitch( info.strip.strip() ) * info.fractionalError, phiDet, &clus};
    }
  };
  struct VeloLiteR : VeloLiteBase, VeloRBase {
    static LHCb::Measurement makeMeasurement( LHCb::VeloLiteCluster const& clus, DeVelo const& det,
                                              IVeloClusterPosition::toolInfo const& info ) {
      DeVeloRType const* rDet = det.rSensor( clus.channelID().sensor() );
      return {rDet->z(), rDet->trajectory( info.strip, info.fractionalPosition ),
              info.fractionalError * rDet->rPitch( info.strip.strip() ), rDet, clus};
    }
  };
  struct VeloLitePhi : VeloLiteBase, VeloPhiBase {
    static LHCb::Measurement makeMeasurement( LHCb::VeloLiteCluster const& clus, DeVelo const& det,
                                              IVeloClusterPosition::toolInfo const& info ) {
      DeVeloPhiType const* phiDet = det.phiSensor( clus.channelID() );
      return {phiDet->z(), phiDet->trajectory( info.strip, info.fractionalPosition ),
              info.fractionalError * phiDet->phiPitch( info.strip.strip() ), phiDet, clus};
    }
  };

  //////////// VP
  struct VP {
    using PositionToolType = IVPClusterPosition;
    static std::string positionToolName() { return "VPClusterPosition"; }

    using DetectorType = DeVP;
    static std::string const& defaultDetectorLocation() { return DeVPLocation::Default; }
    static double nominalZ( DetectorType const& det, LHCb::LHCbID id ) { return det.sensor( id.vpID() )->z(); }

    using Cluster = LHCb::VPLightCluster;

    using ClusterContainerType = LHCb::VPLightClusters;
    static std::string const& defaultClusterLocation() { return LHCb::VPClusterLocation::Light; }

    static LHCb::VPChannelID channelId( LHCb::LHCbID id ) { return id.vpID(); }

    static LHCb::Measurement makeMeasurement( LHCb::VPLightCluster const& clus, DeVP const& det,
                                              LHCb::VPPositionInfo info, bool localY ) {
      Gaudi::XYZPoint position( info.x, info.y, clus.z() );
      const auto*     sensor = det.sensor( clus.channelID().sensor() );
      if ( localY ) {
        return {clus.channelID(), clus.z(),
                LHCb::LineTraj<double>{position, LHCb::Trajectory<double>::Vector{1, 0, 0},
                                       LHCb::Trajectory<double>::Range{-info.dx, info.dx},
                                       LHCb::Trajectory<double>::DirNormalized{true}},
                info.dy, sensor};
      } else {
        return {clus.channelID(), clus.z(),
                LHCb::LineTraj<double>{position, LHCb::Trajectory<double>::Vector{0, 1, 0},
                                       LHCb::Trajectory<double>::Range{-info.dy, info.dy},
                                       LHCb::Trajectory<double>::DirNormalized{true}},
                info.dx, sensor};
      }
    }
  };

  //////////// ST
  struct STBase {
    using PositionToolType = ISTClusterPosition;
    using DetectorType     = DeSTDetector;
    static LHCb::STChannelID channelId( LHCb::LHCbID id ) { return id.stID(); }
  };

  struct STFullBase : STBase {
    using Cluster              = LHCb::STCluster;
    using ClusterContainerType = LHCb::STClusters;
    static LHCb::Measurement makeMeasurement( LHCb::STCluster const& clus, DeSTDetector const& det,
                                              ISTClusterPosition const& posTool ) {
      const DeSTSector* stSector = det.findSector( clus.channelID() );
      auto              measVal  = posTool.estimate( &clus );
      return {stSector->globalCentre().z(),
              stSector->trajectory( measVal.strip, measVal.fractionalPosition ),
              measVal.fractionalError * stSector->pitch(),
              stSector,
              &clus,
              measVal.clusterSize};
    }
  };
  struct STLiteBase : STBase {
    using Cluster              = LHCb::STLiteCluster;
    using ClusterContainerType = LHCb::STLiteCluster::STLiteClusters;
    static LHCb::Measurement makeMeasurement( LHCb::STLiteCluster const& clus, DeSTDetector const& det,
                                              ISTClusterPosition const& posTool ) {
      const DeSTSector* stSector = det.findSector( clus.channelID() );
      return {stSector->globalCentre().z(), stSector->trajectory( clus.channelID(), clus.interStripFraction() ),
              posTool.error( clus.pseudoSize() ) * stSector->pitch(), stSector, clus};
    };
  };

  struct TTBase {
    static std::string const& defaultDetectorLocation() { return DeSTDetLocation::TT; }
    static bool               checkType( LHCb::LHCbID id ) { return id.isTT(); }
  };
  struct ITBase {
    static std::string const& defaultDetectorLocation() { return DeSTDetLocation::IT; }
    static bool               checkType( LHCb::LHCbID id ) { return id.isIT(); }
  };

  struct TT : STFullBase, TTBase {
    static std::string        positionToolName() { return "STOfflinePosition/TTClusterPosition"; }
    static std::string const& defaultClusterLocation() { return LHCb::STClusterLocation::TTClusters; }
  };
  struct TTLite : STLiteBase, TTBase {
    static std::string        positionToolName() { return "STOnlinePosition/TTLiteClusterPosition"; }
    static std::string const& defaultClusterLocation() { return LHCb::STLiteClusterLocation::TTClusters; }
  };

  struct UTLite {
    using PositionToolType = IUTClusterPosition;
    static std::string positionToolName() { return "UTOnlinePosition/UTLiteClusterPosition"; }
    using DetectorType = DeUTDetector;
    static std::string const& defaultDetectorLocation() { return DeUTDetLocation::UT; }
    using Cluster              = UT::Hit;
    using ClusterContainerType = UT::HitHandler;
    static std::string const& defaultClusterLocation() { return UT::Info::HitLocation; }

    static LHCb::UTChannelID channelId( LHCb::LHCbID id ) { return id.utID(); }

    static LHCb::Measurement makeMeasurement( UT::Hit const& clus, DeUTDetector const& det,
                                              IUTClusterPosition const& positiontool ) {
      const DeUTSector* utSector = det.findSector( clus.chanID() );
      return {clus.lhcbID(), utSector->globalCentre().z(), utSector->trajectory( clus.chanID(), clus.fracStrip() ),
              positiontool.error( clus.pseudoSize() ) * utSector->pitch(), utSector};
    }
  };

  struct IT : STFullBase, ITBase {
    static std::string        positionToolName() { return "STOfflinePosition/ITClusterPosition"; }
    static std::string const& defaultClusterLocation() { return LHCb::STClusterLocation::ITClusters; }
  };
  struct ITLite : STLiteBase, ITBase {
    static std::string        positionToolName() { return "STOnlinePosition/ITLiteClusterPosition"; }
    static std::string const& defaultClusterLocation() { return LHCb::STLiteClusterLocation::ITClusters; }
  };

} // namespace MeasurementProviderTypes
namespace {
  template <typename T>
  const auto* id2cluster( LHCb::LHCbID id, typename T::ClusterContainerType const& clusters ) {
    if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
      const auto clus = LIKELY( id.isVP() )
                            ? std::find_if( clusters.begin(), clusters.end(),
                                            [cid = id.vpID().channelID()]( LHCb::VPLightCluster const& clus ) {
                                              return clus.channelID() == cid;
                                            } )
                            : clusters.end();
      return clus != clusters.end() ? &*clus : nullptr;
    } else if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ) {
      auto  cid  = id.utID();
      auto& hits = clusters.hits( cid.station(), cid.layer(), cid.detRegion(), cid.sector() );
      auto  hit = std::find_if( hits.begin(), hits.end(), [cid]( UT::Hit const& hit ) { return hit.chanID() == cid; } );
      return hit != hits.end() ? &*hit : nullptr;
    } else {
      return LIKELY( T::checkType( id ) ) ? clusters.object( T::channelId( id ) ) : nullptr;
    }
  }
} // namespace

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------
template <typename T>
LHCb::Measurement MeasurementProviderT<T>::measurement( typename T::Cluster const& clus,
                                                        [[maybe_unused]] bool      localY ) const {
  if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
    return T::makeMeasurement( clus, *m_det, m_positiontool->position( clus ), localY );
  } else if constexpr ( std::is_base_of_v<MeasurementProviderTypes::VeloBase, T> ) {
    return T::makeMeasurement( clus, *m_det, m_positiontool->position( &clus ) );
  } else if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ||
                        std::is_base_of_v<MeasurementProviderTypes::STBase, T> ) {
    return T::makeMeasurement( clus, *m_det, *m_positiontool );
  }
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------
template <typename T>
LHCb::Measurement MeasurementProviderT<T>::measurement( typename T::Cluster const&       clus,
                                                        LHCb::ZTrajectory<double> const& reftraj,
                                                        [[maybe_unused]] bool            localY ) const {
  if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ||
                 std::is_base_of_v<MeasurementProviderTypes::STBase, T> ) { // ST & UTLite ignores reference
                                                                            // trajectory...
    return measurement( clus, localY );
  } else {
    if ( UNLIKELY( !m_useReference ) ) return measurement( clus, localY );
    LHCb::StateVector sv = reftraj.stateVector( T::nominalZ( *m_det, clus.channelID() ) );
    if constexpr ( std::is_same_v<T, MeasurementProviderTypes::VP> ) {
      return T::makeMeasurement( clus, *m_det, m_positiontool->position( clus, sv.position(), sv.tx(), sv.ty() ),
                                 localY );
    } else if constexpr ( std::is_base_of_v<MeasurementProviderTypes::VeloBase, T> ) {
      return T::makeMeasurement( clus, *m_det,
                                 m_positiontool->position( &clus, sv.position(), std::pair{sv.tx(), sv.ty()} ) );
    }
  }
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

template <typename T>
void MeasurementProviderT<T>::addToMeasurements( LHCb::span<LHCb::LHCbID>         ids,
                                                 std::vector<LHCb::Measurement>&  measurements,
                                                 LHCb::ZTrajectory<double> const& reftraj ) const {
  measurements.reserve( measurements.size() + ids.size() );
  auto to_clus = [clusters = m_clustersDH.get()]( LHCb::LHCbID id ) { return id2cluster<T>( id, *clusters ); };
  std::transform( ids.begin(), ids.end(), std::back_inserter( measurements ), [&]( LHCb::LHCbID id ) {
    auto clus = to_clus( id );
    assert( clus != nullptr );
    return this->measurement( *clus, reftraj, false );
  } );
}

template <>
void MeasurementProviderT<MeasurementProviderTypes::VP>::addToMeasurements(
    LHCb::span<LHCb::LHCbID> ids, std::vector<LHCb::Measurement>& measurements,
    LHCb::ZTrajectory<double> const& ref ) const {
  measurements.reserve( measurements.size() + 2 * ids.size() );
  auto to_clus = [clusters = m_clustersDH.get()]( LHCb::LHCbID id ) {
    return id2cluster<MeasurementProviderTypes::VP>( id, *clusters );
  };
  std::for_each( ids.begin(), ids.end(), [&]( LHCb::LHCbID id ) {
    auto clus = to_clus( id );
    assert( clus != nullptr );
    measurements.push_back( measurement( *clus, ref, false ) );
    measurements.push_back( measurement( *clus, ref, true ) );
  } );
}

using VPMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VP>;
DECLARE_COMPONENT( VPMeasurementProvider )
using UTLiteMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::UTLite>;
DECLARE_COMPONENT( UTLiteMeasurementProvider )
