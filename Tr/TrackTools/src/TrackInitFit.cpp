/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "TrackInitFit.h"

// from TrackEvent
#include "Event/State.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TrackInitFit
//
// 2009-11-14 : Kostyantyn Holubyev
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackInitFit )

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackInitFit::initialize() {

  info() << "==> Initialize " << endmsg;

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  m_initTrack = tool<ITrackStateInit>( m_initToolName, "Init", this );
  m_fitTrack  = tool<ITrackFitter>( m_fitToolName, "Fit", this );

  return StatusCode::SUCCESS;
}

//=============================================================================
