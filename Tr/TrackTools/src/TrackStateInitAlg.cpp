/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackStateInitAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TrackStateInitAlg
//
// 2009-03-02 : Pavel Krokovny
//-----------------------------------------------------------------------------

#ifdef _WIN32
#  pragma warning( disable : 4355 ) // This used in initializer list, needed for ToolHandles
#endif

DECLARE_COMPONENT( TrackStateInitAlg )

TrackStateInitAlg::TrackStateInitAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "StateInitTool", m_trackTool );
}

StatusCode TrackStateInitAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;
  sc = m_trackTool.retrieve();
  return sc;
}

StatusCode TrackStateInitAlg::finalize() {
  m_trackTool.release().ignore();
  return GaudiAlgorithm::finalize();
}

StatusCode TrackStateInitAlg::execute() {
  LHCb::Tracks* tracks = get<LHCb::Tracks>( m_trackLocation );
  for ( LHCb::Tracks::const_iterator it = tracks->begin(); tracks->end() != it; ++it ) {
    LHCb::Track* track = *it;
    StatusCode   sc    = m_trackTool->fit( *track, clearStates );
    if ( sc.isFailure() ) {
      // Warn any downstream code that this track is bad
      track->setFitStatus( LHCb::Track::FitStatus::FitFailed );
      Warning( "TrackStateInitTool fit failed", sc, 0 ).ignore();
    }
  }
  return StatusCode::SUCCESS;
}
