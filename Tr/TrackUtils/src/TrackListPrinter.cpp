/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// track interfaces
#include "Event/Track.h"

/** @class TrackListPrinter TrackListPrinter.h
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

class TrackListPrinter final : public GaudiAlgorithm {
public:
  // Constructors and destructor
  TrackListPrinter( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;

private:
  std::string m_inputLocation;
};

DECLARE_COMPONENT( TrackListPrinter )

TrackListPrinter::TrackListPrinter( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // constructor
  declareProperty( "InputLocation", m_inputLocation = LHCb::TrackLocation::Default );
}

StatusCode TrackListPrinter::execute() {
  for ( const auto& t : get<LHCb::Track::Range>( m_inputLocation ) ) info() << *t << endmsg;
  return StatusCode::SUCCESS;
}
