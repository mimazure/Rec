/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UpgradeGhostIdNT_H
#define UpgradeGhostIdNT_H 1

// Include files
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/ToolHandle.h"
#include "MCInterfaces/ITrackGhostClassification.h"
#include "TrackInterfaces/IGhostProbability.h"

/*@class UpgradeGhostIdNT UpgradeGhostIdNT.h

  To Generate ntuples for the ghost probability study

*/
class UpgradeGhostIdNT : public GaudiTupleTool, virtual public IGhostProbability {
public:
  /// Standard constructor
  UpgradeGhostIdNT( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode                       initialize() override; ///< Algorithm initialization
  virtual StatusCode               execute( LHCb::Track& aTrack ) const override;
  virtual StatusCode               beginEvent() override { return m_ghostTool->beginEvent(); };
  virtual std::vector<std::string> variableNames( LHCb::Track::Types type ) const override {
    return m_ghostTool->variableNames( type );
  };
  virtual std::vector<float> netInputs( LHCb::Track& aTrack ) const override {
    return m_ghostTool->netInputs( aTrack );
  };

protected:
private:
  ToolHandle<ITrackGhostClassification> m_classification;
  ToolHandle<IGhostProbability>         m_ghostTool;
};

#endif // UpgradeGhostIdNT_H
