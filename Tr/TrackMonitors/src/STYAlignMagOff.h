/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STYSECTEFF_H
#define STYSECTEFF_H 1

// Include files
#include "TrackMonitorBase.h"

class DeSTDetector;
class DeSTSector;
class DeITDetector;

/** @class STYAlignMagOff STYAlignMagOff.h
 *  ...
 *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-05-31
 */
class STYAlignMagOff : public TrackMonitorBase {
public:
  /// Standard constructor
  STYAlignMagOff( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~STYAlignMagOff(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  bool inSector( const LHCb::State& state, const DeSTSector* sector ) const;

  std::string   m_detType;
  DeSTDetector* m_tracker;
  double        m_xTol;
  double        m_yTol;
  bool          m_global;
  bool          m_xy;
  bool          m_expected;
  bool          m_printsectorinfo;
};
#endif // STYSECTEFF_H
