/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <tuple>
#include <vector>

#include "DumpVPGeometry.h"
#include <GaudiKernel/SystemOfUnits.h>
#include <VPDet/VPGeometry.h>

#include <boost/numeric/conversion/cast.hpp>

DECLARE_COMPONENT( DumpVPGeometry )

DumpUtils::Dumps DumpVPGeometry::dumpGeometry() const {

  auto const& det = detector();
  VPGeometry  vp( det );

  DumpUtils::Writer  output{};
  auto const&        sensors         = det.sensors();
  const size_t       sensorPerModule = 4;
  std::vector<float> zs( sensors.size() / sensorPerModule, 0.f );
  auto               sensorZ = [&zs, sensorPerModule]( const auto* sensor ) {
    zs[sensor->module()] += boost::numeric_cast<float>( sensor->z() / sensorPerModule );
  };
  std::for_each( sensors.begin(), sensors.end(), sensorZ );

  output.write( zs.size(), zs, vp.m_local_x.size(), vp.m_local_x, vp.m_x_pitch.size(), vp.m_x_pitch, vp.m_ltg.size(),
                vp.m_ltg[0].size() );
  for ( auto const& ltg : vp.m_ltg ) { output.write( ltg ); }

  return {{std::tuple{output.buffer(), "velo_geometry", Allen::NonEventData::VeloGeometry::id}}};
}
