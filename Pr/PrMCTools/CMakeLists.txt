###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PrMCTools
################################################################################
gaudi_subdir(PrMCTools v2r12)

gaudi_depends_on_subdirs(Det/FTDet
                         Det/VPDet
                         Det/STDet
                         Det/UTDet
                         Event/FTEvent
                         Event/LinkerEvent
                         DAQ/DAQKernel
                         Event/DAQEvent
                         Event/DigiEvent
                         Associators/AssociatorsBase
                         Event/MCEvent
                         GaudiAlg
                         Pr/PrFitParams
                         Pr/PrKernel
                         Pr/PrFitParams
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT COMPONENTS RIO Tree)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(PrMCTools
                 src/*.cpp
                 INCLUDE_DIRS ROOT Event/FTEvent Pr/PrPixel Associators/AssociatorsBase
                 LINK_LIBRARIES ROOT DAQEventLib DAQKernelLib LinkerEvent MCEvent GaudiAlgLib PrKernel VPDetLib STDetLib UTDetLib FTDetLib LoKiMCLib PrFitParamsLib RelationsLib MuonDAQLib)