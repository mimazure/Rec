/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPLANEHYBRIDCOUNTER_H
#define PRPLANEHYBRIDCOUNTER_H 1

// Include files
#include "Kernel/STLExtensions.h"
#include "PrHybridSeedTrack.h"
#include "PrKernel/PrFTHitHandler.h"
/** @class PrPlaneHybridCounter PrPlaneHybridCounter.h
 *  Class to count how many different planes are in a List and how many planes with a single hit fired.
 *  It also checks if the content of hits is distributed in the SciFi in a proper way 1 hit per station X ( and UV ).
 *
 *  @author renato quagliani
 *  @date   2015-07-13
 */

class PrPlaneHybridCounter final {
public:
  PrPlaneHybridCounter() = default;

  /** Set values: given a range of iterators it fills the information for the hit content
   *  @brief Set values for a given range of iterators
   *  @param itBeg First iterator, begin of range
   *  @param itEnd Last iterator, end of range
   *  @param fill Should the values be reset?
   */
  void set( StereoIter itBeg, StereoIter itEnd ) noexcept {
    for ( auto itH = itBeg; itEnd != itH; ++itH ) {
      unsigned int plane = itH->planeCode;
      // 0---3 (T1) , 4--7 (T2), 8--11( T3
      ASSUME( !( ( plane % 4 == 0 ) || ( plane % 4 == 3 ) ) );
      ASSUME( plane / 4 < 3 );
      // if no hits in the plane, increase the counters anyway
      if ( 0 == m_planeList[plane]++ ) {
        m_nbT[plane / 4]++;
        m_nbT_singleHit[plane / 4]++;
      } else if ( 2 == m_planeList[plane] ) { // you have already increased it in the >0 condition, never from ==0
        m_nbT_singleHit[plane / 4]--;
      }
    } // end loop hits
  }

  /** Set values: given a range of iterators it fills the information for the hit content
   *  @brief Set values for a given range of iterators
   *  @param itBeg First iterator, begin of range
   *  @param itEnd Last iterator, end of range
   *  @param fill Should the values be reset?
   */
  void set( HitIter itBeg, HitIter itEnd ) noexcept {
    for ( auto itH = itBeg; itEnd != itH; ++itH ) {
      // 0---3 (T1) , 4--7 (T2), 8--11( T3
      unsigned int plane = itH->planeCode;
      ASSUME( !( ( plane % 4 == 0 ) || ( plane % 4 == 3 ) ) );
      ASSUME( plane / 4 < 3 );

      // if no hits in the plane, increase the counters anyway
      if ( 0 == m_planeList[plane]++ ) {
        m_nbT[plane / 4]++;
        m_nbT_singleHit[plane / 4]++;
      } else if ( 2 == m_planeList[plane] ) { // you have already increased it in the >0 condition, never from ==0
        m_nbT_singleHit[plane / 4]--;
      }
    } // end loop hits
  }

  void addHit( const ModPrHit& hit ) noexcept {
    unsigned int plane = hit.hit->planeCode();
    ASSUME( !( ( plane % 4 == 0 ) || ( plane % 4 == 3 ) ) );
    ASSUME( plane / 4 < 3 );

    if ( 0 == m_planeList[plane]++ ) {
      m_nbT[plane / 4]++;
      m_nbT_singleHit[plane / 4]++;
      // optimization with the elseif possible here ( not used anyway)
    } else if ( 2 == m_planeList[plane] ) { // if you are adding a hit where already present one singlecounting
      m_nbT_singleHit[plane / 4]--;
    }
  }

  void removeHit( const ModPrHit& hit ) noexcept {
    unsigned int plane = hit.planeCode;
    ASSUME( !( ( plane % 4 == 0 ) || ( plane % 4 == 3 ) ) );
    ASSUME( plane / 4 < 3 );

    if ( 0 == --m_planeList[plane] ) { // you remain with 0 hits in that plane decrease everything
      m_nbT[plane / 4]--;
      m_nbT_singleHit[plane / 4]--;
    } else if ( 1 == m_planeList[plane] ) // you remain with just one hit in that layer after removing it
    {
      m_nbT_singleHit[plane / 4]++;
    }
  }
  unsigned int nbDifferent() const noexcept { return m_nbT[0] + m_nbT[1] + m_nbT[2]; }
  unsigned int nbSingle() const noexcept { return m_nbT_singleHit[0] + m_nbT_singleHit[1] + m_nbT_singleHit[2]; }
  unsigned int nbHits() const noexcept { return std::accumulate( m_planeList.begin(), m_planeList.end(), 0 ); }

  bool isOK() const noexcept {
    // at least 1 UV hit in each station & at least one station with 2 UV hits
    return ( ( m_nbT[0] >= 1 && m_nbT[1] >= 1 && m_nbT[2] >= 1 ) && ( nbHits() > 3 ) );
  }

  unsigned int nbInPlane( const int plane ) const noexcept { return m_planeList[plane]; }
  //  unsigned int nbInPlane( int plane) const noexcept{ return m_planeList[plane];}
  void removeHitInPlane( unsigned int plane ) noexcept {
    m_planeList[plane]--;
    return;
  }

private:
  std::array<unsigned int, 12> m_planeList{};

  unsigned int m_nbT[3]{};
  unsigned int m_nbT_singleHit[3]{};
};
#endif // PRPLANEHYBRIDCOUNTER_H
