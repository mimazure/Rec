/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

#include "VeloClusterTracking.h"
#include <immintrin.h>

#include "VPSortByPhi_avx256.h"

namespace VeloTracking {

  inline __mmask8 _mm256_mask_cmplt_ps_mask( __mmask8 k1, __m256 a, __m256 b ) {
    return _mm256_mask_cmp_ps_mask( k1, a, b, _CMP_LT_OS );
  }

  inline __mmask8 _mm256_mask_cmple_ps_mask( __mmask8 k1, __m256 a, __m256 b ) {
    return _mm256_mask_cmp_ps_mask( k1, a, b, _CMP_LE_OS );
  }

  inline __mmask8 _mm256_cmplt_ps_mask( __m256 a, __m256 b ) { return _mm256_cmp_ps_mask( a, b, _CMP_LT_OS ); }

  inline __m256i _mm256_load_epi32( void* a ) { return _mm256_load_si256( (__m256i*)a ); }

  inline __m256i _mm256_setzero_epi32() { return _mm256_setzero_si256(); }

  inline __m256i _mm256_or_epi32( __m256i a, __m256i b ) { return _mm256_or_si256( a, b ); }

  inline __m256i _mm256_and_epi32( __m256i a, __m256i b ) { return _mm256_and_si256( a, b ); }

  inline int _mm256_mask_reduce_or_epi32( __mmask8 k, __m256i a ) {
    a         = _mm256_maskz_mov_epi32( k, a );
    __m128i r = _mm_max_epi32( _mm256_extractf128_si256( a, 0 ), _mm256_extractf128_si256( a, 1 ) );
    r         = _mm_or_si128( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
    r         = _mm_or_si128( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
    return _mm_extract_epi32( r, 0 );
  }

  inline int _mm256_mask_reduce_max_epi32( __mmask8 mask, __m256i a ) {
    a         = _mm256_maskz_mov_epi32( mask, a );
    __m128i r = _mm_max_epi32( _mm256_extractf128_si256( a, 0 ), _mm256_extractf128_si256( a, 1 ) );
    r         = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
    r         = _mm_max_epi32( r, _mm_shuffle_epi32( r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
    return _mm_extract_epi32( r, 0 );
  }

  inline float _mm256_reduce_max_ps( __m256 a ) {
    __m128 r = _mm_max_ps( _mm256_extractf128_ps( a, 0 ), _mm256_extractf128_ps( a, 1 ) );
    r        = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 2, 3, 0, 1 ) ) );
    r        = _mm_max_ps( r, _mm_shuffle_ps( r, r, _MM_SHUFFLE( 1, 0, 3, 2 ) ) );
    return _mm_cvtss_f32( r );
  }

  inline __attribute__( ( always_inline ) ) void sort_by_phi( PlaneSoA* Pin, PlaneSoA* Pout, int*, int n_hits ) {
    for ( int i = 0; i < n_hits; i += 8 ) {
      __m256 key = compute_key( Pin, i );
      _mm256_store_ps( &( Pout->phi[i] ), key );
    }

    quicksort_avx256( Pout->phi, 0, n_hits - 1 );

    for ( int i = 0; i < n_hits; i += 8 ) {
      __m256 key = _mm256_load_ps( &( Pout->phi[i] ) );
      permute_hits( Pin, Pout, key, i );
    }
  }

  inline void set_used_256( PlaneSoA* P, __mmask8 m, __m256i h ) {
    __m256i mask = _mm256_sllv_epi32( _mm256_set1_epi32( 1 ), _mm256_and_si256( h, _mm256_set1_epi32( 31 ) ) );
    __m256i perm = _mm256_srli_epi32( h, 5 );
    alignas( 32 ) int imask[8];
    alignas( 32 ) int iperm[8];

    _mm256_store_si256( (__m256i*)imask, mask );
    _mm256_store_si256( (__m256i*)iperm, perm );

    for ( int i = 0; i < 8; i++ ) {
      if ( ( m >> i ) & 1 ) { ( (uint32_t*)P->used )[iperm[i]] |= imask[i]; }
    }
  }

  inline __m256 diff_phi( __m256 phi1, __m256 phi2 ) { return _mm256_abs_ps( _mm256_sub_ps( phi1, phi2 ) ); }

  inline __attribute__( ( always_inline ) ) int TrackSeeding( PlaneSoA* P0, PlaneSoA* P1, PlaneSoA* P2,
                                                              LightTracksSoA* tracks, // Output track candidates
                                                              const int       t_insert_start ) {
    int n_tracks = 0;
    int start0   = 0;
    int start2   = 0;

    const int MAX_H0_CANDIDATES_PER_H1 = 4;
    int8_t    insert_mask[MAX_H0_CANDIDATES_PER_H1];
    __m256    x0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256    y0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256    z0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m256i   h0_candidates[MAX_H0_CANDIDATES_PER_H1];

    __m256i inc = _mm256_set1_epi32( 8 );
    __m256i vh1 =
        _mm256_add_epi32( _mm256_set1_epi32( P1->offset ), _mm256_setr_epi32( -8, -7, -6, -5, -4, -3, -2, -1 ) );
    for ( int h1 = 0; h1 < P1->n_hits; h1 += 8 ) {
      vh1             = _mm256_add_epi32( vh1, inc );
      __mmask8 used_1 = ( (uint8_t*)P1->used )[h1 / 8];
      if ( ( h1 + 8 ) > P1->n_hits ) used_1 |= 0xFF << ( P1->n_hits & 7 );

      if ( used_1 == 0xFF ) continue;

      const __m256 x1    = _mm256_load_ps( &( P1->Gx[h1] ) );
      const __m256 y1    = _mm256_load_ps( &( P1->Gy[h1] ) );
      const __m256 z1    = _mm256_load_ps( &( P1->Gz[h1] ) );
      const __m256 phi1  = _mm256_load_ps( &( P1->phi[h1] ) );
      const float  phi1f = ( ( h1 + 8 ) > P1->n_hits ) ? P1->phi[P1->n_hits - 1] : P1->phi[h1 + 7]; // largest phi in h1

      __m256   bestFit = _mm256_set1_ps( TrackParams::max_scatter_seeding );
      __mmask8 bestH0  = 0;
      __m256i  bestH2  = _mm256_setzero_epi32();
      __m256i  vh0     = _mm256_setzero_epi32();
      __m256i  vh2     = _mm256_setzero_epi32();

      __m256 bestX0 = _mm256_setzero_ps();
      __m256 bestY0 = _mm256_setzero_ps();
      __m256 bestZ0 = _mm256_setzero_ps();
      __m256 bestX2 = _mm256_setzero_ps();
      __m256 bestY2 = _mm256_setzero_ps();
      __m256 bestZ2 = _mm256_setzero_ps();

      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) insert_mask[i] = 0; // reset candidates masks

      // Fill h0 candidates TETRIS-style
      for ( int h0 = start0; h0 < P0->n_hits; h0++ ) {
        if ( is_used( P0, h0 ) ) continue;

        const __m256i cid0 = _mm256_set1_epi32( P0->offset + h0 );
        const __m256  x0   = _mm256_set1_ps( P0->Gx[h0] );
        const __m256  y0   = _mm256_set1_ps( P0->Gy[h0] );
        const __m256  z0   = _mm256_set1_ps( P0->Gz[h0] );

        const float  phi0f = P0->phi[h0];
        const __m256 phi0  = _mm256_set1_ps( phi0f );
        const __m256 dphi  = diff_phi( phi1, phi0 );
        __mmask8     m_phi = _mm256_mask_cmple_ps_mask( ~used_1, dphi, _mm256_set1_ps( TrackParams::phi_window ) );
        if ( m_phi == 0 ) {
          if ( phi1f < phi0f ) break;
          start0 = h0 + 1;
          continue;
        }

        // h0 is a candidate
        for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
          __mmask8 can_store = m_phi & ( ~insert_mask[i] );
          insert_mask[i] |= m_phi;

          x0_candidates[i] = _mm256_mask_mov_ps( x0_candidates[i], can_store, x0 );
          y0_candidates[i] = _mm256_mask_mov_ps( y0_candidates[i], can_store, y0 );
          z0_candidates[i] = _mm256_mask_mov_ps( z0_candidates[i], can_store, z0 );
          h0_candidates[i] = _mm256_mask_mov_epi32( h0_candidates[i], can_store, cid0 );

          m_phi &= ~can_store;
          if ( m_phi == 0 ) break;
        }
      }

      int next_start2 = start2; // start2 cannot be updated while all h0 candidates havent been processed
      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
        __mmask8 m_phi = insert_mask[i];
        if ( m_phi == 0 ) break;

        const __m256  x0 = x0_candidates[i];
        const __m256  y0 = y0_candidates[i];
        const __m256  z0 = z0_candidates[i];
        const __m256i h0 = h0_candidates[i];

        for ( int h2 = start2; h2 < P2->n_hits; h2++ ) {
          const float  phi2f  = P2->phi[h2];
          const __m256 phi2   = _mm256_set1_ps( phi2f );
          const __m256 dphi   = diff_phi( phi1, phi2 );
          __mmask8     m_phi2 = _mm256_mask_cmple_ps_mask( ~used_1, dphi, _mm256_set1_ps( TrackParams::phi_window ) );
          if ( m_phi2 == 0 ) {
            if ( phi1f < phi2f ) break;
            next_start2 = h2 + 1;
            continue;
          }

          m_phi2 &= m_phi;
          if ( m_phi2 == 0 ) continue;

          const __m256i cid2 = _mm256_set1_epi32( P2->offset + h2 );
          const __m256  x2   = _mm256_set1_ps( P2->Gx[h2] );
          const __m256  y2   = _mm256_set1_ps( P2->Gy[h2] );
          const __m256  z2   = _mm256_set1_ps( P2->Gz[h2] );

          const __m256 vr = _mm256_div_ps( _mm256_sub_ps( z2, z0 ), _mm256_sub_ps( z1, z0 ) );
          const __m256 x  = _mm256_fmadd_ps( _mm256_sub_ps( x1, x0 ), vr, x0 );
          const __m256 y  = _mm256_fmadd_ps( _mm256_sub_ps( y1, y0 ), vr, y0 );

          const __m256 dx      = _mm256_sub_ps( x, x2 );
          const __m256 dy      = _mm256_sub_ps( y, y2 );
          const __m256 dz      = _mm256_sub_ps( z2, z1 );
          const __m256 scDen2  = _mm256_mul_ps( dz, dz );
          const __m256 scatter = _mm256_div_ps( _mm256_fmadd_ps( dx, dx, _mm256_mul_ps( dy, dy ) ), scDen2 );

          __mmask8 m_tol =
              _mm256_mask_cmplt_ps_mask( m_phi2, _mm256_abs_ps( dx ), _mm256_set1_ps( TrackParams::tolerance ) );
          m_tol = _mm256_mask_cmplt_ps_mask( m_tol, _mm256_abs_ps( dy ), _mm256_set1_ps( TrackParams::tolerance ) );

          __mmask8 m_bestfit = _mm256_mask_cmplt_ps_mask( m_tol, scatter, bestFit );

          if ( m_bestfit == 0 ) continue;

          bestFit = _mm256_mask_mov_ps( bestFit, m_bestfit, scatter );
          bestX0  = _mm256_mask_mov_ps( bestX0, m_bestfit, x0 );
          bestY0  = _mm256_mask_mov_ps( bestY0, m_bestfit, y0 );
          bestZ0  = _mm256_mask_mov_ps( bestZ0, m_bestfit, z0 );
          bestX2  = _mm256_mask_mov_ps( bestX2, m_bestfit, x2 );
          bestY2  = _mm256_mask_mov_ps( bestY2, m_bestfit, y2 );
          bestZ2  = _mm256_mask_mov_ps( bestZ2, m_bestfit, z2 );
          bestH2  = _mm256_mask_mov_epi32( bestH2, m_bestfit, _mm256_set1_epi32( h2 ) );
          vh0     = _mm256_mask_mov_epi32( vh0, m_bestfit, h0 );
          vh2     = _mm256_mask_mov_epi32( vh2, m_bestfit, cid2 );

          bestH0 |= m_bestfit;
        } // h2
      }   // n_0_candidates
      start2 = next_start2;

      // Clone killing: require that h2 are different
      __m256i ch2 = _mm256_maskz_conflict_epi32( bestH0, vh2 );
      bestH0      = _mm256_mask_cmpeq_epi32_mask( bestH0, ch2, _mm256_setzero_epi32() );

      if ( bestH0 == 0 ) continue;

      int i = t_insert_start + n_tracks;

      // only h2 has scattering
      _mm256_mask_compressstoreu_ps( &( tracks->sum_scatter[i] ), bestH0, bestFit );

      _mm256_mask_compressstoreu_ps( &( tracks->x0[i] ), bestH0, bestX0 );
      _mm256_mask_compressstoreu_ps( &( tracks->y0[i] ), bestH0, bestY0 );
      _mm256_mask_compressstoreu_ps( &( tracks->z0[i] ), bestH0, bestZ0 );

      _mm256_mask_compressstoreu_ps( &( tracks->x1[i] ), bestH0, x1 );
      _mm256_mask_compressstoreu_ps( &( tracks->y1[i] ), bestH0, y1 );
      _mm256_mask_compressstoreu_ps( &( tracks->z1[i] ), bestH0, z1 );

      _mm256_mask_compressstoreu_ps( &( tracks->x2[i] ), bestH0, bestX2 );
      _mm256_mask_compressstoreu_ps( &( tracks->y2[i] ), bestH0, bestY2 );
      _mm256_mask_compressstoreu_ps( &( tracks->z2[i] ), bestH0, bestZ2 );

      _mm256_mask_compressstoreu_ps( &( tracks->phi2[i] ), bestH0, phi1 ); // Cheat a little: phi1 ~= phi2

      _mm256_mask_compressstoreu_epi32( &( tracks->n_hits[i] ), bestH0, _mm256_set1_epi32( 3 ) );
      _mm256_mask_compressstoreu_epi32( &( tracks->skipped[i] ), bestH0, _mm256_setzero_epi32() );

      n_tracks += _mm_popcnt_u32( bestH0 );
      ( (uint8_t*)P1->used )[h1 / 8] |= bestH0;
      set_used_256( P2, bestH0, bestH2 );

      if ( TrackParams::save_LHCbIds ) {
        _mm256_mask_compressstoreu_epi32( &( tracks->cId[i] ), bestH0, vh0 );
        _mm256_mask_compressstoreu_epi32( &( tracks->cId[MAX_TRACKS_PER_EVENT + i] ), bestH0, vh1 );
        _mm256_mask_compressstoreu_epi32( &( tracks->cId[2 * MAX_TRACKS_PER_EVENT + i] ), bestH0, vh2 );
      }
    } // h1

    return n_tracks;
  }

  inline __attribute__( ( always_inline ) ) void TrackForwarding( LightTracksSoA* tracks, // Track candidates
                                                                  const int n_tracks, PlaneSoA* P2,
                                                                  LightTracksSoA* tracks_forwarded, int& n_forwarded,
                                                                  LightTracksSoA* tracks_finalized, int& n_finalized ) {
    for ( int t = 0; t < n_tracks; t += 8 ) {
      __mmask8 mask = ( ( t + 8 ) > n_tracks ) ? ~( 0xFF << ( n_tracks & 7 ) ) : 0xFF;

      __m256 x0   = _mm256_load_ps( &( tracks->x1[t] ) );
      __m256 y0   = _mm256_load_ps( &( tracks->y1[t] ) );
      __m256 z0   = _mm256_load_ps( &( tracks->z1[t] ) );
      __m256 x1   = _mm256_load_ps( &( tracks->x2[t] ) );
      __m256 y1   = _mm256_load_ps( &( tracks->y2[t] ) );
      __m256 z1   = _mm256_load_ps( &( tracks->z2[t] ) );
      __m256 phi1 = _mm256_load_ps( &( tracks->phi2[t] ) );

      float phi1f = _mm256_reduce_max_ps( phi1 ); // largest phi in tracks

      __m256   bestFit = _mm256_set1_ps( TrackParams::max_scatter_forwarding );
      __mmask8 bestH0  = 0;
      __m256i  bestH2  = _mm256_setzero_epi32();
      __m256   bestX2  = x1;
      __m256   bestY2  = y1;
      __m256   bestZ2  = z1;
      __m256   bestPhi = phi1;
      __m256i  vh2     = _mm256_setzero_epi32();

      for ( int h2 = 0; h2 < P2->n_hits; h2++ ) {
        if ( is_used( P2, h2 ) ) continue;

        float    phi2f  = P2->phi[h2];
        __m256   phi2   = _mm256_set1_ps( phi2f );
        __m256   dphi   = diff_phi( phi1, phi2 );
        __mmask8 m_phi2 = _mm256_mask_cmple_ps_mask( mask, dphi, _mm256_set1_ps( TrackParams::phi_window ) );
        if ( m_phi2 == 0 ) {
          if ( phi1f < phi2f ) break;
          continue; // cannot restart from start2, because tracks not ordered
        }

        __m256i cid2 = _mm256_set1_epi32( P2->offset + h2 );
        __m256  x2   = _mm256_set1_ps( P2->Gx[h2] );
        __m256  y2   = _mm256_set1_ps( P2->Gy[h2] );
        __m256  z2   = _mm256_set1_ps( P2->Gz[h2] );

        __m256 vr = _mm256_div_ps( _mm256_sub_ps( z2, z0 ), _mm256_sub_ps( z1, z0 ) );
        __m256 x  = _mm256_fmadd_ps( _mm256_sub_ps( x1, x0 ), vr, x0 );
        __m256 y  = _mm256_fmadd_ps( _mm256_sub_ps( y1, y0 ), vr, y0 );

        __m256       dx      = _mm256_sub_ps( x, x2 );
        __m256       dy      = _mm256_sub_ps( y, y2 );
        __m256       dz      = _mm256_sub_ps( z2, z1 );
        const __m256 scDen2  = _mm256_mul_ps( dz, dz );
        const __m256 scatter = _mm256_div_ps( _mm256_fmadd_ps( dx, dx, _mm256_mul_ps( dy, dy ) ), scDen2 );

        __mmask8 m_tol =
            _mm256_mask_cmplt_ps_mask( m_phi2, _mm256_abs_ps( dx ), _mm256_set1_ps( TrackParams::tolerance ) );
        m_tol = _mm256_mask_cmplt_ps_mask( m_tol, _mm256_abs_ps( dy ), _mm256_set1_ps( TrackParams::tolerance ) );

        __mmask8 m_bestfit = _mm256_mask_cmplt_ps_mask( m_tol, scatter, bestFit );

        if ( m_bestfit == 0 ) continue;

        bestFit = _mm256_mask_mov_ps( bestFit, m_bestfit, scatter );
        bestX2  = _mm256_mask_mov_ps( bestX2, m_bestfit, x2 );
        bestY2  = _mm256_mask_mov_ps( bestY2, m_bestfit, y2 );
        bestZ2  = _mm256_mask_mov_ps( bestZ2, m_bestfit, z2 );
        bestPhi = _mm256_mask_mov_ps( bestPhi, m_bestfit, phi2 );
        bestH2  = _mm256_mask_mov_epi32( bestH2, m_bestfit, _mm256_set1_epi32( h2 ) );
        vh2     = _mm256_mask_mov_epi32( vh2, m_bestfit, cid2 );

        bestH0 |= m_bestfit;
      }

      // Finish loading tracks
      __m256  tx0     = _mm256_load_ps( &( tracks->x0[t] ) );
      __m256  ty0     = _mm256_load_ps( &( tracks->y0[t] ) );
      __m256  tz0     = _mm256_load_ps( &( tracks->z0[t] ) );
      __m256i n_hits  = _mm256_load_epi32( &( tracks->n_hits[t] ) );
      __m256i skipped = _mm256_load_epi32( &( tracks->skipped[t] ) );

      __m256 ssc = _mm256_load_ps( &( tracks->sum_scatter[t] ) );

      // increment or reset to 0
      skipped = _mm256_maskz_add_epi32( ~bestH0, skipped, _mm256_set1_epi32( 1 ) );

      // increment only if we found a hit
      n_hits = _mm256_mask_add_epi32( n_hits, bestH0, n_hits, _mm256_set1_epi32( 1 ) );
      ssc    = _mm256_mask_add_ps( ssc, bestH0, ssc, bestFit );

      x1 = _mm256_mask_mov_ps( x0, bestH0, x1 );
      y1 = _mm256_mask_mov_ps( y0, bestH0, y1 );
      z1 = _mm256_mask_mov_ps( z0, bestH0, z1 );

      __mmask8 m_forward =
          _mm256_mask_cmple_epi32_mask( mask, skipped, _mm256_set1_epi32( TrackParams::max_allowed_skip ) );

      // Mark used
      set_used_256( P2, bestH0 & m_forward, bestH2 );

      // Forward tracks
      int i = n_forwarded;

      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->sum_scatter[i] ), m_forward, ssc );

      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->x0[i] ), m_forward, tx0 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->y0[i] ), m_forward, ty0 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->z0[i] ), m_forward, tz0 );

      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->x1[i] ), m_forward, x1 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->y1[i] ), m_forward, y1 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->z1[i] ), m_forward, z1 );

      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->x2[i] ), m_forward, bestX2 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->y2[i] ), m_forward, bestY2 );
      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->z2[i] ), m_forward, bestZ2 );

      _mm256_mask_compressstoreu_ps( &( tracks_forwarded->phi2[i] ), m_forward, bestPhi );

      _mm256_mask_compressstoreu_epi32( &( tracks_forwarded->n_hits[i] ), m_forward, n_hits );
      _mm256_mask_compressstoreu_epi32( &( tracks_forwarded->skipped[i] ), m_forward, skipped );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_forward, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i  cid      = _mm256_load_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          __mmask8 push_hit = _mm256_mask_cmpeq_epi32_mask( bestH0, n_hits, _mm256_set1_epi32( j + 1 ) );
          cid               = _mm256_mask_mov_epi32( cid, push_hit, vh2 );
          _mm256_mask_compressstoreu_epi32( &( tracks_forwarded->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_forward, cid );
        }
      }

      n_forwarded += _mm_popcnt_u32( m_forward );

      // Finalize track
      __mmask8 m_final = _mm256_cmpge_epi32_mask( n_hits, _mm256_set1_epi32( TrackParams::min_hit_per_track ) );
      m_final |= _mm256_cmplt_ps_mask( ssc, _mm256_set1_ps( TrackParams::max_scatter_3hits ) );
      m_final &= mask & ~m_forward;

      if ( m_final == 0 ) continue; // Nothing to finalize

      i = n_finalized;

      _mm256_mask_compressstoreu_ps( &( tracks_finalized->sum_scatter[i] ), m_final, ssc );

      _mm256_mask_compressstoreu_ps( &( tracks_finalized->x0[i] ), m_final, tx0 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->y0[i] ), m_final, ty0 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->z0[i] ), m_final, tz0 );

      _mm256_mask_compressstoreu_ps( &( tracks_finalized->x1[i] ), m_final, x1 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->y1[i] ), m_final, y1 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->z1[i] ), m_final, z1 );

      _mm256_mask_compressstoreu_ps( &( tracks_finalized->x2[i] ), m_final, bestX2 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->y2[i] ), m_final, bestY2 );
      _mm256_mask_compressstoreu_ps( &( tracks_finalized->z2[i] ), m_final, bestZ2 );

      _mm256_mask_compressstoreu_ps( &( tracks_finalized->phi2[i] ), m_final, bestPhi );

      _mm256_mask_compressstoreu_epi32( &( tracks_finalized->n_hits[i] ), m_final, n_hits );
      _mm256_mask_compressstoreu_epi32( &( tracks_finalized->skipped[i] ), m_final, skipped );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i cid = _mm256_load_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm256_mask_compressstoreu_epi32( &( tracks_finalized->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_final, cid );
        }
      }

      n_finalized += _mm_popcnt_u32( m_final );
    }
  }

  inline __attribute__( ( always_inline ) ) void copy_remaining( LightTracksSoA* tracks_candidates, int n_candidates,
                                                                 LightTracksSoA* tracks, int& n_tracks_output ) {
    __m256i min_hit_per_track = _mm256_set1_epi32( TrackParams::min_hit_per_track );
    __m256  max_scatter_3hits = _mm256_set1_ps( TrackParams::max_scatter_3hits );
    for ( int t = 0; t < n_candidates; t += 8 ) {
      __mmask8 mask = ( ( t + 8 ) > n_candidates ) ? ~( 0xFF << ( n_candidates & 7 ) ) : 0xFF;

      __m256i n_hits = _mm256_load_epi32( &( tracks_candidates->n_hits[t] ) );
      __m256  ssc    = _mm256_load_ps( &( tracks_candidates->sum_scatter[t] ) );

      __mmask8 m_final = _mm256_mask_cmpge_epi32_mask( mask, n_hits, min_hit_per_track );
      m_final |= _mm256_mask_cmplt_ps_mask( mask, ssc, max_scatter_3hits );

      int i = n_tracks_output;
      _mm256_mask_compressstoreu_ps( &( tracks->sum_scatter[i] ), m_final, ssc );

      _mm256_mask_compressstoreu_ps( &( tracks->x0[i] ), m_final, _mm256_load_ps( &( tracks_candidates->x0[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->y0[i] ), m_final, _mm256_load_ps( &( tracks_candidates->y0[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->z0[i] ), m_final, _mm256_load_ps( &( tracks_candidates->z0[t] ) ) );

      _mm256_mask_compressstoreu_ps( &( tracks->x1[i] ), m_final, _mm256_load_ps( &( tracks_candidates->x1[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->y1[i] ), m_final, _mm256_load_ps( &( tracks_candidates->y1[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->z1[i] ), m_final, _mm256_load_ps( &( tracks_candidates->z1[t] ) ) );

      _mm256_mask_compressstoreu_ps( &( tracks->x2[i] ), m_final, _mm256_load_ps( &( tracks_candidates->x2[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->y2[i] ), m_final, _mm256_load_ps( &( tracks_candidates->y2[t] ) ) );
      _mm256_mask_compressstoreu_ps( &( tracks->z2[i] ), m_final, _mm256_load_ps( &( tracks_candidates->z2[t] ) ) );

      _mm256_mask_compressstoreu_ps( &( tracks->phi2[i] ), m_final, _mm256_load_ps( &( tracks_candidates->phi2[t] ) ) );

      _mm256_mask_compressstoreu_epi32( &( tracks->n_hits[i] ), m_final, n_hits );
      _mm256_mask_compressstoreu_epi32( &( tracks->skipped[i] ), m_final,
                                        _mm256_load_epi32( &( tracks_candidates->skipped[t] ) ) );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm256_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m256i cid = _mm256_load_epi32( &( tracks_candidates->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm256_mask_compressstoreu_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_final, cid );
        }
      }

      n_tracks_output += _mm_popcnt_u32( m_final );
    }
  }
} // namespace VeloTracking
