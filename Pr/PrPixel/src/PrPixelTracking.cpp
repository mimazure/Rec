/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/StateParameters.h"
#include "Event/Track_v2.h"
#include "Kernel/VPConstants.h"
// Local
#include "PrPixelTracking.h"
#include <VPDet/VPDetPaths.h>
#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::PixelTracking, "PrPixelTracking" )

namespace LHCb::Pr::Velo {

  namespace {
    using namespace ranges;
    inline float maxPhiModule( unsigned moduleID, const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                               span<const float> phi_hits ) {
      return phi_hits[offsets[moduleID + 1] - 1];
    }

    inline float minPhiModule( unsigned moduleID, const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                               span<const float> phi_hits ) {
      return phi_hits[offsets[moduleID]];
    }
  } // namespace

  namespace VPConf {
    /* Those are the string matching of configuration you can use and it change the behaviour of Pairs creations
              "Default"             : default pixel tracking
              "OnlyForward"         : search only for dr/dz >0 tracks with hits having z> MinZ_ForwardTracks
              "OnlyBackward"        : search only for dr/dz <0 tracks with hits having z< MaxZ_BackwardTracks
              "ForwardThenBackward" : Do the OnlyForward  first and OnlyBackward after
              "BackwardThenForward" : Do the OnlyBackward first and OnlyForward after
              "whateverelse"        : No tracking
    */
    namespace {
      static constexpr auto configuration =
          std::array{std::pair{ConfAlgo::DefaultAlgo, "Default"}, std::pair{ConfAlgo::OnlyForward, "OnlyForward"},
                     std::pair{ConfAlgo::OnlyBackward, "OnlyBackward"},
                     std::pair{ConfAlgo::ForwardThenBackward, "ForwardThenBackward"},
                     std::pair{ConfAlgo::BackwardThenForward, "BackwardThenForward"}};
    }
    std::string toString( const ConfAlgo& scheme ) {
      auto i = std::find_if( begin( configuration ), end( configuration ),
                             [&]( const std::pair<ConfAlgo, const char*>& p ) { return p.first == scheme; } );
      if ( i == end( configuration ) ) {
        throw std::range_error( "Invalid VPConf::ConfAlgo" );
        return "<<<INVALID>>>";
      }
      return i->second;
    }

    StatusCode parse( ConfAlgo& result, std::string_view input ) {
      if ( input.size() > 1 && input.front() == input.back() && ( input.front() == '"' || input.front() == '\'' ) ) {
        // strip optional quotes around strings
        input = input.substr( 1, input.size() - 2 );
      }
      auto i = std::find_if( begin( configuration ), end( configuration ),
                             [&]( const std::pair<ConfAlgo, const char*>& p ) { return p.second == input; } );
      if ( i == end( configuration ) ) return StatusCode::FAILURE;
      result = i->first;
      return StatusCode::SUCCESS;
    }
  } // namespace VPConf

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  PixelTracking::PixelTracking( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator,
                          {KeyValue{"ClusterLocation", VPClusterLocation::Light},
                           KeyValue{"ClusterOffsets", VPClusterLocation::Offsets}},
                          {KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
                           KeyValue{"OutputTracksName", "Rec/Track/Velo"}} ) {
    if ( !setProperty( "VPTrackingInfo", m_vpInfo.key() + '-' + name ) )
      throw GaudiException( "failed to set VPTrackingInfo property", name, StatusCode::FAILURE );
  }

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode PixelTracking::initialize() {

    StatusCode sc = MultiTransformer::initialize();
    if ( sc.isFailure() ) return sc;

    // declare the condition derivation
    const bool withAlignment = exist<Condition>( detSvc(), LHCb::Det::VP::module_align );
    VPTrackingInfo::registerDerivation( conditionDerivationMgr(), m_vpInfo.key(), withAlignment, m_ForwardTracks_minZ,
                                        m_BackwardTracks_maxZ );

    for ( unsigned int moduleNumber : m_modulesToSkip ) {
      m_modulesToSkipMask.set( moduleNumber );
      m_modulesToSkipForPairsMask.set( moduleNumber );
    }
    for ( unsigned int moduleNumber : m_modulesToSkipForPairs ) { m_modulesToSkipForPairsMask.set( moduleNumber ); }
    // always set Histo top dir if we are debugging histos
#ifdef DEBUG_HISTO
    setHistoTopDir( "VP/" );
#endif

    return StatusCode::SUCCESS;
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<Tracks, Tracks> PixelTracking::
                             operator()( const EventContext& ctx, const std::vector<VPLightCluster>& clusters,
              const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets ) const {

    const auto& vpInfo = m_vpInfo.get( getConditionContext( ctx ) );

    //---- Get for each cluster the phi value, clusters have been already sorted in VPClus algorithm
    const std::vector<float> phi_hits     = getPhi( clusters, offsets );
    auto                     outputTracks = searchByPair( vpInfo, clusters, offsets, phi_hits );

    m_tracksCounter += std::get<0>( outputTracks ).size() + std::get<1>( outputTracks ).size();
    m_clustersCounter += clusters.size();
    return outputTracks;
  }

  //============================================================================
  // Get phi values of the hits from the clusters
  //============================================================================

  std::vector<float> PixelTracking::getPhi( span<const VPLightCluster>                        clusters,
                                            span<const unsigned, VeloInfo::Numbers::NOffsets> offsets ) const {
    std::vector<float> phiVector( clusters.size(), 0.f );
    // For odd modules, no branching, no swap, no offset.
    for ( size_t moduleID = 1; moduleID < VeloInfo::NModules; moduleID += 2 ) {
      for ( size_t hit = offsets[moduleID]; hit < offsets[moduleID + 1]; ++hit ) {
        phiVector[hit] = atan2_approximation1( clusters[hit].y(), clusters[hit].x() );
      }
    }
    // For even  modules, branching , add swap by default , use offset
    for ( size_t moduleID = 0; moduleID < VeloInfo::NModules; moduleID += 2 ) {
      for ( size_t hit = offsets[moduleID]; hit < offsets[moduleID + 1]; ++hit ) {
        const float phi = atan2_approximation1( clusters[hit].y(), clusters[hit].x() );
        phiVector[hit]  = phi + ( phi < 0.f ? 360.f : 0.f ); // sorted by increasing phi. We can save the if for phi<0.
      }
    }
    return phiVector;
  }

  //=============================================================================
  // Extend track towards smaller/larger Modules depending on algo configuration
  // Control flow in extendTrack depends on this small method
  //=============================================================================
  template <SearchDirection configuration>
  inline void PixelTracking::updatenextstationsearch( unsigned int& next, int value ) const {
    switch ( configuration ) {
    case SearchDirection::Default:
      next -= value;
      break; // move jumping on module placed in the backward direction
    case SearchDirection::Forward:
      next -= value;
      break; // move jumping on module placed in the backward direction
    case SearchDirection::Backward:
      next += value;
      break; // move jumping on module placed in the forward direction
    default:
      throw( "update next station search , impossible configuration " );
    }
    return;
  }

  //=============================================================================
  // Extend track towards smaller z,
  // on both sides of the detector as soon as one hit is missed.
  //=============================================================================
  template <SearchDirection configuration>
  void PixelTracking::extendTrack( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                                   const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                   boost::container::static_vector<size_t, 35>& hitbuffer, unsigned int lastmoduleID,
                                   span<const float> phi_hits, span<unsigned char> clusterIsUsed ) const {

    // Initially scan every second module (stay on the same side).
    int step = -2;
    // Start two modules behind the last one.
    if constexpr ( configuration == SearchDirection::Backward ) { step = +2; }
    // Count modules without hits found.
    // lastmoduleID keeps track of the last index to the module closest in z to the
    // one used to seed the bestHit call
    unsigned int foundHits          = 2;
    unsigned int missed_consecutive = 0;
    unsigned int missed_OnTrack     = 0;
#ifdef DEBUG_LOGIC
    std::vector<unsigned int> modules_touched = {lastmoduleID, lastmoduleID + step};
    auto CheckingModuleSequence = [&]( std::vector<unsigned int>& modulesChecked, unsigned int checked_now ) {
      if ( std::find( modulesChecked.begin(), modulesChecked.end(), checked_now ) != modulesChecked.end() ) {
        std::cout << "Checking MID " << checked_now << " again Series : " << std::flush;
        for ( auto cc : modulesChecked ) { std::cout << cc << " , " << std::flush; }
        std::cout << std::endl;
      } else {
        modulesChecked.push_back( checked_now );
      }
    };
#endif
    unsigned int next;
    // 1 Missed counted when ( next - prev  > 2 )
    while ( lastmoduleID >= 2 && missed_consecutive <= m_maxMissedConsecutive &&
            missed_OnTrack <= m_maxMissedOnTrack ) {
      // always try first same side w.r.t last hit moduleID added on track, this is the most probable case.
      next = lastmoduleID + step;

      if constexpr ( configuration == SearchDirection::Forward ) {
        // Extend the Forward Tracks up to the last Module at z > m_minZForwardTracks
        if ( lastmoduleID < vpInfo.m_modulesForwardLoop.back() ) break;
      }
      if constexpr ( configuration == SearchDirection::Backward ) {
        // Extend the Backward tracks ( eta < 0 ) up to the last Module at z < m_maxZBackwardTracks
        if ( lastmoduleID > vpInfo.m_modulesBackwardLoop.back() ) break;
      }

      if ( m_modulesToSkipMask[next] ) {
        lastmoduleID += step;
        continue;
      }
      // if( configuration == SearchDirection::Backward && next > m_lastModuleBackward) break;
      // if( configuration == SearchDirection::Forward && next  < m_lastModuleForward ) break;
      size_t h3 = badhit;
      if ( m_boostPhysics ) {
        // Boost physics in both forward and backward track seach
        h3 = bestHit<AddHitMode::ExactExtimation>( vpInfo, clusters, offsets, lastmoduleID, foundHits, next, hitbuffer,
                                                   phi_hits, clusterIsUsed );
      } else {
        h3 = bestHit<AddHitMode::SameSide>( vpInfo, clusters, offsets, lastmoduleID, foundHits, next, hitbuffer,
                                            phi_hits, clusterIsUsed );
      }
      if ( h3 < badhit ) {
        ++foundHits;
        hitbuffer.push_back( h3 );
        missed_consecutive = 0;
        lastmoduleID       = next;
        continue;
      } else {
        // you have not find a hit in the same side module, try the other side, closest in z to prev
        // subtract (add) 1 to next for forward (backward). (go further in z )
        if ( m_boostPhysics && missed_consecutive == 0 ) {
          updatenextstationsearch<configuration>( next, -1 ); // forward : next -> (next-2) +1
#ifdef DEBUG_LOGIC
          CheckingModuleSequence( modules_touched, next );
#endif
          h3 = bestHit<AddHitMode::ExactExtimation>( vpInfo, clusters, offsets, lastmoduleID, foundHits, next,
                                                     hitbuffer, phi_hits, clusterIsUsed );
          if ( h3 < badhit ) {
            ++foundHits;
            hitbuffer.push_back( h3 );
            missed_consecutive = 0;
            lastmoduleID       = next;
            missed_consecutive = 0;
            continue;
          } else {
            updatenextstationsearch<configuration>( next, +1 );
          }
        }
        updatenextstationsearch<configuration>( next, +1 ); // next -> (next -2 -1)
        if ( m_modulesToSkipMask[next] ) {                  // protect if next you try is in the list to skip
          lastmoduleID += step;
          continue;
        }
        if ( m_boostPhysics ) {
#ifdef DEBUG_LOGIC
          CheckingModuleSequence( modules_touched, next );
#endif
          h3 = bestHit<AddHitMode::ExactExtimation>( vpInfo, clusters, offsets, lastmoduleID, foundHits, next,
                                                     hitbuffer, phi_hits, clusterIsUsed );
        } else {
#ifdef DEBUG_LOGIC
          CheckingModuleSequence( modules_touched, next );
#endif
          h3 = bestHit<AddHitMode::ChangeSide>( vpInfo, clusters, offsets, lastmoduleID, foundHits, next, hitbuffer,
                                                phi_hits, clusterIsUsed );
        }
        if ( h3 < badhit ) {
          ++foundHits;
          hitbuffer.push_back( h3 );
          missed_consecutive = 0;
          lastmoduleID       = next;
          continue;
        } else {
          // increase counters of missing moudules (+1 means you missed both left and right)
          missed_OnTrack++;
          missed_consecutive++;
          // If you were trying to extend a doublet and you have not found anything at first trial;
          // break here, too large jump otherwisen
          if ( m_earlykill3hittracks && foundHits == 2 ) break;
          if ( m_boostPhysics && foundHits == 2 ) break;
          updatenextstationsearch<configuration>( lastmoduleID, 2 );
          continue;
        }
      }
    }
  }
  //=========================================================================
  //  Search starting with a pair of consecutive modules.
  //=========================================================================
  std::tuple<Tracks, Tracks>
  PixelTracking::searchByPair( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                               const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                               span<const float>                                        phi_hits ) const {

    std::tuple<Tracks, Tracks> outputTracks;

    // cache 3-hit tracks to only fit if the hits are really unused after the pattern reco is done
    // If the hardFlagging is enabled, ThreeHitVec will never be filled. (it should never be)
    std::vector<size_t> ThreeHitVec;
    ThreeHitVec.reserve( m_hardFlagging ? 1 : 300 );
    // vector storing whether hits are used. 0 means unused, other value is used
    std::vector<unsigned char> clusterIsUsed( clusters.size(), 0 );
    // allocate a buffer used for temporary storage of the tracks we are building
    // it is allocated here and passed to doPairSearch in order to reuse the
    // memory and avoid new allocation for each track, most of the tracks will have less hits
    boost::container::static_vector<size_t, 35> hitbuffer{};
    // allocate a PixelTrack object that will be reused for all tracks, again
    // optimizing memory allocations
    PixelTrack FitTrack{35};

    switch ( m_ConfAlgo ) {
    case VPConf::ConfAlgo::DefaultAlgo:
      // do default pattern reco
      doPairSearch<SearchDirection::Default>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                              FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesDefaultLoop );
      break;
    case VPConf::ConfAlgo::OnlyForward:
      // do onlyforward pattern reco
      doPairSearch<SearchDirection::Forward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                              FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesForwardLoop );
      break;
    case VPConf::ConfAlgo::OnlyBackward:
      // do onlybackward pattern reco
      doPairSearch<SearchDirection::Backward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                               FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesBackwardLoop );
      break;
    case VPConf::ConfAlgo::ForwardThenBackward:
      // do forward pattern reco then backward
      doPairSearch<SearchDirection::Forward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                              FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesForwardLoop );
      ThreeHitVec.clear();
      doPairSearch<SearchDirection::Backward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                               FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesBackwardLoop );
      break;
    case VPConf::ConfAlgo::BackwardThenForward:
      // do backward pattern reco then backward
      doPairSearch<SearchDirection::Backward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                               FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesBackwardLoop );
      ThreeHitVec.clear();
      doPairSearch<SearchDirection::Forward>( vpInfo, clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec,
                                              FitTrack, hitbuffer, phi_hits, vpInfo.m_modulesForwardLoop );
      break;
    default:
      throw( "impossible algorithm configuration" );
      break;
    }
    return outputTracks;
  }

  template <SearchDirection configuration>
  void PixelTracking::doPairSearch( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                                    span<unsigned char>                                      clusterIsUsed,
                                    const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                    std::tuple<Tracks, Tracks>& outputTracks, std::vector<size_t>& ThreeHitVec,
                                    PixelTrack& FitTrack, boost::container::static_vector<size_t, 35>& hitbuffer,
                                    span<const float> phi_hits, span<const unsigned int> modulesToLoop ) const {

    int          step     = +2; // step for same side look-up
    unsigned int loopsens = 0;  // keep track of left->right (0->1)
    switch ( configuration ) {
    case SearchDirection::Default:
      step = -2;
      break;
    case SearchDirection::Forward:
      step = -2;
      break;
    case SearchDirection::Backward:
      step = +2;
      break;
    default:
      std::runtime_error( "Please specify correct SearchDirection" );
      break;
    }
    for ( unsigned int iterSens0 = 0; iterSens0 < modulesToLoop.size() - 4; ++iterSens0 ) {
      const unsigned int sens0 = modulesToLoop[iterSens0];
      const unsigned int sens1 = sens0 + step;
      const unsigned int sens2 = sens1 + step;
      if ( m_modulesToSkipForPairsMask[sens0] || m_modulesToSkipForPairsMask[sens1] ||
           m_modulesToSkipForPairsMask[sens2] ) {
        continue;
      }
#ifdef DEBUG_LOGIC
      if ( configuration == SearchDirection::Backward ) {
        info() << "BACKWARD: sens 0 = " << sens0 << "  sens1 " << sens1 << " sens2 " << sens2 << endmsg;
      }
      if ( configuration == SearchDirection::Forward ) {
        info() << "FORWARD: sens 0 = " << sens0 << "  sens1 " << sens1 << " sens2 " << sens2 << endmsg;
      }
#endif
      float tolerancePhi = m_PhiPairs.value();
      if ( configuration == SearchDirection::Forward ) {
        if ( m_boostPhysics && m_usePhiPerRegionForward ) {
          if ( sens0 > m_ForwardRegionModulesID[2] ) {
            tolerancePhi = m_PhiWindowsForwardPerRegions[3];
          } else if ( sens0 > m_ForwardRegionModulesID[1] ) {
            tolerancePhi = m_PhiWindowsForwardPerRegions[2];
          } else if ( sens0 > m_ForwardRegionModulesID[0] ) {
            tolerancePhi = m_PhiWindowsForwardPerRegions[1];
          } else {
            tolerancePhi = m_PhiWindowsForwardPerRegions[0];
          }
        }
      }

      const float z0 = vpInfo.m_moduleZPositions[sens0];
      const float z1 = vpInfo.m_moduleZPositions[sens1];

      const float dz    = z0 - z1;
      const float invdz = 1.f / dz;
      // Calculate the search window from the slope limits.
      const float dxMax = m_maxXSlope * std::abs( dz );
      const float dyMax = m_maxYSlope * std::abs( dz );
      // Loop over hits in the first module (larger Z) in the pair.
      auto firstHit1    = offsets[sens1];
      auto pastLastHit1 = offsets[sens1 + 1];

      for ( size_t hit0 = offsets[sens0]; hit0 < offsets[sens0 + 1]; ++hit0 ) {
        if ( clusterIsUsed[hit0] ) continue;
        const float phi0    = phi_hits[hit0];
        const float phi_min = phi0 - tolerancePhi;
        const float phi_max = phi0 + tolerancePhi;

        if ( m_earlykill3hittracks ) {
          // We may can gain time caching those windows for next, since we move in a sorted vector.
          // Actually phi windows are relatively small, so one can even gain moving linearly to find the min/max
          // (next/next-next)
          auto max_next =
              std::upper_bound( phi_hits.begin() + offsets[sens1], phi_hits.begin() + offsets[sens1 + 1], phi_max );
          auto min_next = std::lower_bound( phi_hits.begin() + offsets[sens1], max_next, phi_min );

          const size_t nbhits_next = max_next - min_next;
          if ( nbhits_next == 0 ) continue;
          auto max_next_next = std::upper_bound( phi_hits.begin() + offsets[sens1 + step],
                                                 phi_hits.begin() + offsets[sens1 + step + 1], phi_max );
          auto min_next_next = std::lower_bound( phi_hits.begin() + offsets[sens1 + step], max_next_next, phi_min );
          const size_t nbhits_next_next = max_next_next - min_next_next;
          if ( nbhits_next_next == 0 ) continue;
        }
        const float y0 = clusters[hit0].y();
        const float x0 = clusters[hit0].x();
        for ( size_t hit1 = firstHit1; hit1 < pastLastHit1; ++hit1 ) {
          const float phi1 = phi_hits[hit1];
          if ( phi1 < phi_min ) {
            firstHit1 = hit1 + 1;
            continue;
          }
          if ( phi1 > phi_max ) break;

          if ( clusterIsUsed[hit1] ) continue;
          const float x1 = clusters[hit1].x();
          const float y1 = clusters[hit1].y();
          // Apply dr/dz cut
          const float drdz_sign = x1 * ( x0 - x1 ) * invdz + y1 * ( y0 - y1 ) * invdz;
          if ( configuration == SearchDirection::Forward ) {
            if ( m_doDrDzCut ) {
              if ( drdz_sign < 0.f ) continue;
            }
          }
          if ( configuration == SearchDirection::Backward ) {
            if ( m_doDrDzCut ) {
              if ( drdz_sign > 0.f ) continue;
            }
          }
          // Skip hits outside the X-pos. limit.
          if ( std::abs( x1 - x0 ) > dxMax ) continue;
          if ( std::abs( y1 - y0 ) > dyMax ) continue;
          hitbuffer.clear();
          hitbuffer.push_back( hit0 );
          hitbuffer.push_back( hit1 );

          extendTrack<configuration>( vpInfo, clusters, offsets, hitbuffer, sens1, phi_hits, clusterIsUsed );

          if ( hitbuffer.size() < 3 ) { continue; }

          if ( hitbuffer.size() == 3 && !m_hardFlagging ) {
            ThreeHitVec.insert( ThreeHitVec.end(), hitbuffer.begin(), hitbuffer.begin() + 3 );
            continue;
          }
          // All other tracks and the case of not hard Flagging
          unsigned unUsed = 0;
          for ( size_t hit : hitbuffer ) {
            if ( !clusterIsUsed[hit] ) ++unUsed;
          }
          if ( unUsed < hitbuffer.size() * m_fractionUnused.value() ) {
            // number of un-used hits is >= fractionUnused * nb hits , keep the track
            continue;
          }
          // 3 hit track and hard flagging.
          if ( hitbuffer.size() == 3 && m_hardFlagging ) {
            if ( unUsed != 3 ) {
              continue;
            } else {
              // fill the 3 hit tracks as well, flag hits if passing chi2 cut
              FitTrack.fill( clusters, hitbuffer );
              FitTrack.fit();
              if ( FitTrack.chi2PerDoF() > m_maxChi2Short.value() ) { continue; }
              for ( size_t hit : hitbuffer ) { clusterIsUsed[hit] = 1; }
              makeLHCbTracks<configuration>( FitTrack, hitbuffer, clusters, outputTracks );
              break;
            }
          }
          // for (const auto idx : hitbuffer) IsUsedVec.set( idx );
          FitTrack.fill( clusters, clusterIsUsed, hitbuffer );
          FitTrack.fit();
          makeLHCbTracks<configuration>( FitTrack, hitbuffer, clusters, outputTracks );
          break;
        } // hit1
      }   // hit0
      if ( m_skiploopsens ) {
        if ( loopsens == 1 ) {
          if ( sens0 >= 44 ) {
            continue;
          } else {
            iterSens0 += 2;
            loopsens = 0;
            continue;
          }
        } else {
          loopsens++;
          continue;
        }
      }
    }

    // Deal with the 3 hit vector here

    for ( size_t i = 0; i < ThreeHitVec.size(); i += 3 ) {
      const size_t h0 = ThreeHitVec[i];
      const size_t h1 = ThreeHitVec[i + 1];
      const size_t h2 = ThreeHitVec[i + 2];
      if ( clusterIsUsed[h0] || clusterIsUsed[h1] || clusterIsUsed[h2] ) continue;
      FitTrack.fill( clusters, h0, h1, h2 );
      FitTrack.fit();
      if ( FitTrack.chi2PerDoF() > m_maxChi2Short.value() ) continue;
      hitbuffer.clear();
      for ( size_t j = 0; j < 3; ++j ) {
        hitbuffer.push_back( ThreeHitVec[i + j] );
        clusterIsUsed[ThreeHitVec[i + j]] = 1; // Flag hits on this 3 hit track , will skip stuff later for used
      }
      // for 3 hit vector ariseing from backward and forward,
      makeLHCbTracks<configuration>( FitTrack, hitbuffer, clusters, outputTracks );
    }
  }

  //=========================================================================
  // Convert the local tracks to LHCb tracks
  //=========================================================================
  template <SearchDirection configuration>
  void
  PixelTracking::makeLHCbTracks( const PixelTrack& track, const boost::container::static_vector<size_t, 35>& hitbuffer,
                                 span<const VPLightCluster> clusters, std::tuple<Tracks, Tracks>& outputTracks ) const {
    // Create a new LHCb track.
    using simd = SIMDWrapper::scalar::types;
    using F    = simd::float_v;
    using Vec  = Vec3<F>;
    // Decide if this is a forward or backward track.
    // Calculate z where the track passes closest to the beam.
    const float zBeam = track.zBeam();
    // Define backward as z closest to beam downstream of hits.
    const bool backward = zBeam > track.hitsZ().front();

    auto& output      = backward ? std::get<0>( outputTracks ) : std::get<1>( outputTracks );
    int   currentsize = output.size();

    {
      // In Forward approach ids already reversed-sorted if lhcbID is increasing for increasing moduleID (direct order
      // for backward tracks)
      std::vector<LHCbID>   ids;
      std::vector<TrackHit> trackHits;
      ids.reserve( hitbuffer.size() );

      if ( configuration == SearchDirection::Forward ) {
        // tracks are created by large z to small z, lhcbID ordered with increasing z, reverse iteration on hit buffer.
        for ( int i = hitbuffer.size(); i-- != 0; ) {
          ids.push_back( clusters[hitbuffer[i]].channelID() );
          output.store_hit( currentsize, i, simd::int_v( hitbuffer[i] ) );
        }
      }
      if ( configuration == SearchDirection::Backward ) {
        // tracks are created by small  to larger z, lhcbID ordered with decreasing z
        for ( size_t i{0}; i < hitbuffer.size(); ++i ) {
          ids.push_back( clusters[hitbuffer[i]].channelID() );
          output.store_hit( currentsize, i, simd::int_v( hitbuffer[i] ) );
        }
      }
      if ( configuration == SearchDirection::Default ) {
        auto tmp = hitbuffer;
        std::sort( tmp.begin(), tmp.end() );
        for ( size_t i{0}; i < tmp.size(); ++i ) {
          ids.push_back( clusters[tmp[i]].channelID() );
          output.store_hit( currentsize, i, simd::int_v( hitbuffer[i] ) );
        }
      }
#ifdef DEBUG_LOGIC
      if ( !std::is_sorted( ids.begin(), ids.end() ) ) {
        error() << "HITS in Velo are not sorted, the track filled is not going to be corrected in terms of LHCbID "
                   "ordering, assumptions are done in the algorithm about that"
                << endmsg;
      }
#endif
    }
    output.store_nHits( currentsize, simd::int_v( hitbuffer.size() ) );

    // Get the state at zBeam from the straight line fit.
    State state;
    state.setLocation( State::Location::ClosestToBeam );
    state.setState( track.state( zBeam ) );
    state.setCovariance( track.covariance( zBeam ) );

    // Parameters for kalmanfit scattering. calibrated on MC, shamelessly
    // hardcoded:
    const float tx    = state.tx();
    const float ty    = state.ty();
    const float scat2 = 1e-8f + 7e-6f * ( tx * tx + ty * ty );

    // The logic is a bit messy in the following, so I hope we got all cases
    // right
    if ( m_stateClosestToBeamKalmanFit ) {
      // Run a K-filter with scattering to improve IP resolution
      State upstreamstate;
      track.fitKalman( upstreamstate, backward ? 1 : -1, scat2 );
      upstreamstate.setLocation( State::Location::ClosestToBeam );
      upstreamstate.linearTransportTo( zBeam );
      output.store_statePos( currentsize, 0, Vec( upstreamstate.x(), upstreamstate.y(), upstreamstate.z() ) );
      output.store_stateDir( currentsize, 0, Vec( upstreamstate.tx(), upstreamstate.ty(), 1.f ) );
      output.store_stateCovX( currentsize, 0,
                              Vec( upstreamstate.covariance()( 0, 0 ), upstreamstate.covariance()( 2, 0 ),
                                   upstreamstate.covariance()( 2, 2 ) ) );
      output.store_stateCovY( currentsize, 0,
                              Vec( upstreamstate.covariance()( 1, 1 ), upstreamstate.covariance()( 3, 1 ),
                                   upstreamstate.covariance()( 3, 3 ) ) );

    } else {
      output.store_statePos( currentsize, 0, Vec( state.x(), state.y(), state.z() ) );
      output.store_stateDir( currentsize, 0, Vec( state.tx(), state.ty(), 1.f ) );
      output.store_stateCovX(
          currentsize, 0, Vec( state.covariance()( 0, 0 ), state.covariance()( 2, 0 ), state.covariance()( 2, 2 ) ) );
      output.store_stateCovY(
          currentsize, 0, Vec( state.covariance()( 1, 1 ), state.covariance()( 3, 1 ), state.covariance()( 3, 3 ) ) );
    }

    // Add state at end of velo
    if ( !backward ) {
      if ( m_stateEndVeloKalmanFit ) {
        State fitstate;
        track.fitKalman( fitstate, backward ? -1 : +1, scat2 );
        state = fitstate;
      }
      state.setLocation( State::Location::EndVelo );
      state.linearTransportTo( StateParameters::ZEndVelo );
      output.store_statePos( currentsize, 1, Vec( state.x(), state.y(), state.z() ) );
      output.store_stateDir( currentsize, 1, Vec( state.tx(), state.ty(), 1.f ) );
      output.store_stateCovX(
          currentsize, 1, Vec( state.covariance()( 0, 0 ), state.covariance()( 2, 0 ), state.covariance()( 2, 2 ) ) );
      output.store_stateCovY(
          currentsize, 1, Vec( state.covariance()( 1, 1 ), state.covariance()( 3, 1 ), state.covariance()( 3, 3 ) ) );
    }

    output.size() += 1;
  }

  //====================================================================================================================================
  // Add hits from the specified module to the track, depends on the Mode to add, ExactExtimation does full atan2
  // computation based on
  //==========================================================================================
  template <AddHitMode mode>
  size_t PixelTracking::bestHit( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                                 const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                 const unsigned int moduleIDlastAdded, const unsigned int foundHits,
                                 const unsigned int next, const boost::container::static_vector<size_t, 35>& hitbuffer,
                                 span<const float> phi_hits, span<unsigned char> clusterIsUsed ) const {
    // Idx of the hit in the clusters container
    size_t firstHit = offsets[next];
    size_t endHit   = offsets[next + 1];
    // empty module
    if ( UNLIKELY( endHit == firstHit ) ) return badhit;
    // Last Hit added on the track is in the hitbuffer at the back position.
    const size_t hit1    = hitbuffer.back();
    float        phi_min = phi_hits[hit1] - m_PhiExtrap.value();
    float        phi_max = phi_hits[hit1] + m_PhiExtrap.value();
    //< this is due to the fact we have sorting by phi inside odd and even module. The branching at pi is moved to a
    // no-branching for a module-side. ( same conversion done in getPhiHit )
    if ( mode == AddHitMode::SameSide ) {
      if ( maxPhiModule( next, offsets, phi_hits ) < phi_min ) return badhit;

      if ( minPhiModule( next, offsets, phi_hits ) > phi_max ) return badhit;
      // All good with phi window, stay same side module, same phi sorting
    }
    if ( mode == AddHitMode::ChangeSide ) {
      float phi1 = phi_hits[hit1];
      // Opply offset value to phi in previous hit (sitting on a different side module of the one under inspection)
      // offset for phi value is needed because the phi-sorting is achieved internally to each module.
      // The values here are obtained looking to crossing-phi module search. You need to change side only if the phi is
      // "close-to boundary" phi within that module, this complication can dissappear
      // if we plug together module X and X-1 treating them as if they have the same z value. TO BE TRIED
      // It would be even more efficient if we have something telling us if that "phi" is in module "next" acceptance.
      // "Dog eating his tail" when crossing left-right or right-left for Odd->Even or Even->Odd modules (boundary at
      // maxPhi sensor or boundary at minPhi sensor)
      float offset = 0.f;
      if ( moduleIDlastAdded % 2 == 0 ) {
        // last module is even and you ar close to end-boundary.
        if ( phi1 > 270.f ) {
          offset = -360.f;
        } else if ( phi1 > 130.f ) {
          // no need to search here [130,270 degrees], you will end-up looking to a faulty region not covered anyway by
          // the module looking for when chaing side !
          return badhit;
        } // otherwise offset remains 0.f
      } else {
        if ( phi1 < -50.f ) {
          offset = +360.f;
        } else if ( phi1 < 90.f ) {
          // no need to search here [-50,90 degrees], you will end-up looking to a faulty region not covered anyway by
          // the module looking for when chaing side !
          return badhit;
        } // otherwise offset remains 0.f
      }
      phi1    = phi1 + offset;
      phi_min = phi1 - m_PhiExtrap.value();
      phi_max = phi1 + m_PhiExtrap.value();
    }

    if ( mode == AddHitMode::ExactExtimation ) {
      const size_t hitbeg = hitbuffer[foundHits - 2]; // to be decided if we want the very first hit in the pair of the
                                                      // current track, or the last hit.
      const float x_beg  = clusters[hitbeg].x();
      const float y_beg  = clusters[hitbeg].y();
      const float z_beg  = clusters[hitbeg].z();
      const float x_last = clusters[hit1].x();
      const float y_last = clusters[hit1].y();
      const float z_last = clusters[hit1].z();
      const float z_next = vpInfo.m_moduleZPositions[next];
      // Compute the expected x,y in next module give the full track
      //( first hit from pair and last added one , compute the tx, ty, predict x,y get phi value for it)
      const float over_dz = 1.f / ( z_last - z_beg );
      const float x_exp   = x_last + ( z_next - z_last ) * ( x_last - x_beg ) * over_dz;
      const float y_exp   = y_last + ( z_next - z_last ) * ( y_last - y_beg ) * over_dz;
      float       phi_exp = atan2_approximation1( y_exp, x_exp );
      phi_exp += ( phi_exp < 0.f && next % 2 == 0 ? 360.f : 0.f );
      phi_min = phi_exp - m_PhiExtrap.value();
      phi_max = phi_exp + m_PhiExtrap.value();
      // The module we touch is outside the phi region we look for (only at around boundaries )
      if ( minPhiModule( next, offsets, phi_hits ) > phi_max ) return badhit;
      if ( maxPhiModule( next, offsets, phi_hits ) < phi_min ) return badhit;
      // You can go faster here....all the computation done afterwards are already done !
    }

    size_t hit_start =
        std::lower_bound( phi_hits.begin() + firstHit, phi_hits.begin() + endHit, phi_min ) - phi_hits.begin();
    // Phi window is small enough to contain very few hits, move linearly
    size_t hit_end = hit_start;
    while ( hit_end != endHit && phi_hits[hit_end] < phi_max ) { hit_end++; }

    if ( hit_end - hit_start == 0 ) return badhit;

    // Here, if you have 1-2 hits in the window probably the next is over-doing stuffs. (with small phi, you may avoid
    // all the computations afterward Now use the Pair of previous hits to do the best Scatter search (short range z
    // extrapolation, if track has bent due to multiple scattering, take the first hit in the pair seed is probably
    // wrong... TO BE revisite
    const size_t hit0 = hitbuffer[foundHits - 2];
    const float  x0   = clusters[hit0].x();
    const float  z0   = clusters[hit0].z();
    const float  x1   = clusters[hit1].x();
    const float  z1   = clusters[hit1].z();
    const float  td   = 1.0f / ( z1 - z0 );
    const float  txn  = ( x1 - x0 );
    const float  tx   = txn * td;
    const float  y0   = clusters[hit0].y();
    const float  y1   = clusters[hit1].y();
    const float  tyn  = ( y1 - y0 );
    const float  ty   = tyn * td;

#ifdef DEBUG_HISTO
    size_t nFound = 0;
#endif

    float  bestScatter = m_maxScatterSq;
    size_t bestHit     = badhit;
    for ( size_t i = hit_start; i < hit_end; ++i ) {
      if ( clusterIsUsed[i] ) continue;
      const float hit_x = clusters[i].x();
      const float hit_z = clusters[i].z();
      const float dz    = hit_z - z0;
      const float xPred = x0 + tx * dz;
#ifdef DEBUG_HISTO
      plot( ( hit.x() - xPred ) / m_extraTol, "HitExtraErrPerTol", "Hit X extrapolation error / tolerance", -4.0, +4.0,
            400 );
#endif
      const float dx = xPred - hit_x;
      // If x-position is above prediction +- tolerance, keep looking (hits sorted by phi)
      if ( std::abs( dx ) > m_extraTol ) continue;
      const float hit_y = clusters[i].y();
      const float yPred = y0 + ty * dz;
      const float dy    = yPred - hit_y;
      // Skip hits outside the y-position tolerance.
      if ( std::abs( dy ) > m_extraTol ) continue;
      const float scatterDenom = 1.0f / ( hit_z - z1 );
      const float scatterNum   = ( dx * dx ) + ( dy * dy );
      const float scatter      = scatterNum * scatterDenom * scatterDenom;
      if ( scatter < bestScatter ) {
        bestHit     = i;
        bestScatter = scatter;
      }
#ifdef DEBUG_HISTO
      if ( scatter < m_maxScatterSq ) ++nFound;
      plot( sqrt( scatter ), "HitScatter", "hit scatter [rad]", 0.0, 0.5, 500 );
      plot2D( dx, dy, "Hit_dXdY", "Difference between hit and prediction in x and y [mm]", -1, 1, -1, 1, 500, 500 );
#endif
    }
#ifdef DEBUG_HISTO
    plot( nFound, "HitExtraCount", "Number of hits within the extrapolation window with chi2 within limits" 0.0, 10.0,
          10 );
#endif
    return bestHit;
  }

  //=========================================================================
  // Print all hits on a track.
  //=========================================================================
  void PixelTracking::printTrack( PixelTrack& track ) const {
    for ( size_t hit = 0; hit < track.size(); ++hit ) {
      info() << format( " x%8.3f y%8.3f z%8.2f", track.hitsX()[hit], track.hitsY()[hit],
                        track.hitsZ()[hit] ); //, track.hitsID()[hit].lhcbID());
      info() << endmsg;
    }
  }
} // namespace LHCb::Pr::Velo
