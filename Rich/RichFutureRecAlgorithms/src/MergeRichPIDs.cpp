/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "MergeRichPIDs.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

MergeRichPIDs::MergeRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
    : MergingTransformer( name, pSvcLocator,
                          // inputs
                          {"InputRichPIDLocations", {}},
                          // outputs
                          {"OutputRichPIDLocation", LHCb::RichPIDLocation::Default} ) {
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

LHCb::RichPIDs MergeRichPIDs::operator()( const vector_of_const_<LHCb::RichPIDs>& inPIDs ) const {

  // the merged PID container
  LHCb::RichPIDs outPIDs;

  // reserve total size
  outPIDs.reserve( std::accumulate( inPIDs.begin(), inPIDs.end(), 0u,
                                    []( const auto sum, const auto& pids ) { return sum + pids.size(); } ) );

  // set the version
  outPIDs.setVersion( (unsigned char)m_pidVersion );

  // Loop over inputs and clone into output
  for ( const auto& pids : inPIDs ) {

    // Warn if the input and meged output containers have different versions
    if ( UNLIKELY( outPIDs.version() != pids.version() ) ) { ++m_verWarn; }

    // loop over PIDs and clone.
    for ( const auto pid : pids ) {

      // Make a new cloned RichPID object.
      auto newPID = std::make_unique<LHCb::RichPID>( *pid );

      // Should we try and preserve the original PID key ?
      if ( m_keepPIDKey ) {
        outPIDs.insert( newPID.get(), pid->key() );
      } else {
        outPIDs.insert( newPID.get() );
      }

      // if get here, insertion was OK so give up ownership
      newPID.release();
    }
  }

  _ri_verbo << "Created " << outPIDs.size() << " RichPIDs : Version " << (unsigned short)outPIDs.version() << endmsg;

  // return the output container
  return outPIDs;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MergeRichPIDs )

//=============================================================================
