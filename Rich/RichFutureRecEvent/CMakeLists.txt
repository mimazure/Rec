###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichFutureRecEvent
################################################################################
gaudi_subdir(RichFutureRecEvent v1r0)

gaudi_depends_on_subdirs(Event/RecEvent
                         Det/RichDet
                         Rich/RichUtils
                         Rich/RichFutureUtils
                         Rich/RichRecUtils)

find_package(Boost)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} 
                           ${Vc_INCLUDE_DIR})

gaudi_add_library(RichFutureRecEvent
                  src/*.cpp
                  PUBLIC_HEADERS RichFutureRecEvent
                  LINK_LIBRARIES RecEvent RichUtils RichFutureUtils RichRecUtils RichDetLib )

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecEvent APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()

# Do we need this with ROOT6 ?
#gaudi_add_dictionary(RichFutureRecEvent
#                     dict/RichFutureRecEvent.h
#                     dict/RichFutureRecEvent.xml
#                     LINK_LIBRARIES RecEvent RichUtils RichFutureUtils RichRecUtils RichFutureRecEvent
#                     OPTIONS "-U__MINGW32__ -DBOOST_DISABLE_ASSERTS")
