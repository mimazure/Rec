/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "RichFutureUtils/RichHypoData.h"

namespace Rich::Future::Rec {

  /// Type for Cherenkov angles
  using CherenkovAngles = Rich::Future::HypoData<float>;

  /// Cherenkov angles TES locations
  namespace CherenkovAnglesLocation {
    /// Location in TES for the emitted photon spectra
    inline const std::string Emitted = "Rec/RichFuture/CherenkovAngles/Emitted";
    /// Location in TES for the signal photon spectra
    inline const std::string Signal = "Rec/RichFuture/CherenkovAngles/signal";
  } // namespace CherenkovAnglesLocation

  /// type for expected Cherenkov resolutions
  using CherenkovResolutions = Rich::Future::HypoData<float>;

  /// Cherenkov resolutions TES locations
  namespace CherenkovResolutionsLocation {
    /// Location in TES for the Cherenkov resolutions
    inline const std::string Default = "Rec/RichFuture/CherenkovResolutions/Default";
  } // namespace CherenkovResolutionsLocation

} // namespace Rich::Future::Rec
