/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//---------------------------------------------------------------------------------
/** @file RichBaseTrSegMaker.h
 *
 *  Header file for tool : Rich::Future::Rec::BaseTrSegMaker
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   14/01/2002
 */
//---------------------------------------------------------------------------------

#pragma once

// STL
#include <array>
#include <vector>

// from Gaudi
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// base class and interface
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event model
#include "Event/State.h"
#include "Event/Track.h"

// LHCbKernel
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRich.h"
#include "RichDet/DeRichBeamPipe.h"
#include "RichDet/DeRichRadiator.h"

// GSL
#include "gsl/gsl_math.h"

namespace Rich::Future::Rec {

  //---------------------------------------------------------------------------------
  /** @class BaseTrSegMaker RichBaseTrSegMaker.h
   *
   *  Base class to tools that create RichTrackSegments from Tracks.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   14/01/2002
   */
  //---------------------------------------------------------------------------------

  class BaseTrSegMaker : public AlgBase {

  public: // Methods for Gaudi Framework
    /// Standard Constructor
    BaseTrSegMaker( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    virtual StatusCode initialize() override;

  protected:
    // methods

    /// Access the DeRichBeamPipe objects, creating as needed on demand
    inline const DeRichBeamPipe* deBeam( const Rich::RadiatorType rad ) const noexcept {
      return deBeam( Rich::Rich2Gas == rad ? Rich::Rich2 : Rich::Rich1 );
    }

    /// Access the DeRichBeamPipe objects, creating as needed on demand
    inline const DeRichBeamPipe* deBeam( const Rich::DetectorType rich ) const noexcept { return m_deBeam[rich]; }

    /// Test flag to see if beam pipe intersections should be checked
    inline bool checkBeamPipe( const Rich::RadiatorType rad ) const noexcept { return m_checkBeamP[rad]; }

    /// Test flag to see if a given radiator is in use
    inline bool usedRads( const Rich::RadiatorType rad ) const noexcept { return m_usedRads[rad]; }

  private:
    // data

    /// RICH beampipe object for each radiator
    DetectorArray<const DeRichBeamPipe*> m_deBeam = {{}};

  private:
    // messaging

    /// Check for beam pipe intersections in each radiator ?
    Gaudi::Property<RadiatorArray<bool>> m_checkBeamP{this, "CheckBeamPipe", {false, true, false}};

    /// Flags to turn on/off individual radiators
    Gaudi::Property<RadiatorArray<bool>> m_usedRads{this, "Radiators", {false, true, true}};
  };

} // namespace Rich::Future::Rec
