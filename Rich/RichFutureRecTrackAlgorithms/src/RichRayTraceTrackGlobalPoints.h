/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Utils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichUtils/RichTrackSegment.h"

// Interfaces
#include "RichFutureInterfaces/IRichRayTracing.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RayTraceTrackGlobalPoints RichRayTraceTrackGlobalPoints.h
   *
   *  Ray traces the track direction through the RICH system to the
   *  HPD panels and stores the results in global coordinates.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceTrackGlobalPoints final
      : public Transformer<SegmentPanelSpacePoints::Vector( const LHCb::RichTrackSegment::Vector& ),
                           Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    RayTraceTrackGlobalPoints( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    virtual StatusCode initialize() override;

  public:
    /// Functional operator
    SegmentPanelSpacePoints::Vector operator()( const LHCb::RichTrackSegment::Vector& segments ) const override;

  private:
    /// Ray tracing tool
    ToolHandle<const IRayTracing> m_rayTrace{this, "RayTracingTool", "Rich::Future::RayTracing/RayTracing"};

    /// Cached trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_traceMode = {{}};

    /// Cached forced trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_fTraceMode = {{}};
  };

} // namespace Rich::Future::Rec
