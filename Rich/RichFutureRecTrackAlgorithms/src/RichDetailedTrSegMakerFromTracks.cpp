/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichDetailedTrSegMakerFromTracks.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

// pull in methods from Rich::RayTracingUtils
using namespace Rich::RayTracingUtils;

//-----------------------------------------------------------------------------
// Implementation file for class : DetailedTrSegMakerFromTracks
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

DetailedTrSegMakerFromTracks::DetailedTrSegMakerFromTracks( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        // inputs
                        {KeyValue{"TracksLocation", LHCb::TrackLocation::Default}},
                        // outputs
                        {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                         KeyValue{"TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial},
                         KeyValue{"SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default}} ) {
  // init
  m_rich.fill( nullptr );
}

//=============================================================================
// Initialisation.
//=============================================================================
StatusCode DetailedTrSegMakerFromTracks::initialize() {

  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // If not using extrapolator, disable it to avoid auto-loading
  if ( m_useStateProvider.value() ) { m_trExt.disable(); }

  // If state provider not required, disable it to avoid auto-loading
  if ( !m_createMissingStates.value() && !m_useStateProvider.value() ) { m_trStateP.disable(); }

  // load MagneticFieldSvc
  m_magFieldSvc = svc<IMagneticFieldSvc>( "MagneticFieldSvc" );

  // get Detector elements for RICH1 and RICH2
  m_rich[Rich::Rich1] = getDet<DeRich>( DeRichLocations::Rich1 );
  m_rich[Rich::Rich2] = getDet<DeRich>( DeRichLocations::Rich2 );

  // Radiators
  m_radiators.reserve( Rich::NRiches );
  if ( usedRads( Rich::Rich1Gas ) ) { m_radiators.push_back( getDet<DeRichRadiator>( DeRichLocations::Rich1Gas ) ); }
  if ( usedRads( Rich::Rich2Gas ) ) { m_radiators.push_back( getDet<DeRichRadiator>( DeRichLocations::Rich2Gas ) ); }

  for ( const auto rad : Rich::radiators() ) { m_minRadLengthSq[rad] = std::pow( m_minRadLength[rad], 2 ); }
  if ( m_useStateProvider.value() ) {
    _ri_debug << "Will use StateProvider instead of extrapolator to move states" << endmsg;
  }

  _ri_debug << "Min radiator path lengths (aero/R1Gas/R2Gas) : " << m_minRadLength << " mm " << endmsg;

  if ( UNLIKELY( m_createMissingStates.value() ) ) {
    warning() << "Will create missing track states using the StateProvider tool. "
              << "If triggered, this will be very slow." << endmsg;
  }

  // Preload Geometry ?
  if ( UNLIKELY( m_preload ) ) {
    auto preloadTool = tool<IGenericTool>( "PreloadGeometryTool" );
    preloadTool->execute();
    release( preloadTool );
  }

  // Force debug messages
  // setProperty( "OutputLevel", MSG::VERBOSE );

  return sc;
}

//=============================================================================

OutData DetailedTrSegMakerFromTracks::operator()( const InData& tracks ) const {

  _ri_debug << "Found " << tracks.size() << " tracks" << endmsg;

  // container to return
  OutData data;

  // shortcuts to tuple contents
  auto& segments    = std::get<LHCb::RichTrackSegment::Vector>( data );
  auto& tkToSegsRel = std::get<Relations::TrackToSegments::Vector>( data );
  auto& segToTkRel  = std::get<Relations::SegmentToTrackVector>( data );

  // GEC cut
  const bool gecAbort = tracks.size() > m_maxTracks;
  // did it fire ?
  if ( UNLIKELY( gecAbort ) ) { ++m_TkGecWarn; }

  // reserve sizes for output containers
  const auto numSegs = tracks.size() * 2; // guess at # segments
  if ( !gecAbort ) { segments.reserve( numSegs ); }
  segToTkRel.reserve( numSegs );
  tkToSegsRel.reserve( tracks.size() );

  // Loop over the input tracks.
  // note we do this anyway, even if GEC fired, as we need to fill the tkToSegsRel container regardless.
  LHCb::Tracks::size_type tkIndex( 0 );
  for ( const auto track : tracks ) {
    if ( track ) { constructSegments( *track, segments, tkIndex, tkToSegsRel, segToTkRel, gecAbort ); }
    ++tkIndex; // must increment for every track in input
  }

  // return the final data
  _ri_debug << "Created " << segments.size() << " track segments" << endmsg;
  return data;
}

//=============================================================================

void DetailedTrSegMakerFromTracks::constructSegments( const LHCb::Track&                  track,       //
                                                      LHCb::RichTrackSegment::Vector&     segments,    //
                                                      const LHCb::Tracks::size_type       tkIndex,     //
                                                      Relations::TrackToSegments::Vector& tkToSegsRel, //
                                                      Relations::SegmentToTrackVector&    segToTkRel,  //
                                                      const bool                          gecAbort     //
                                                      ) const {

  // relations for this track. must be 1 to 1, regardless of how many
  // segments are created for it.
  tkToSegsRel.emplace_back( track.key(), tkIndex );
  auto& tkRels  = tkToSegsRel.back();
  auto& segList = tkRels.segmentIndices;

  // if gec cut fired, abort
  if ( UNLIKELY( gecAbort ) ) return;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Analysing Track key=" << track.key() << " history=" << track.history() << " : " << track.states().size()
            << " States at z =";
    for ( const auto* S : track.states() ) { debug() << " " << S->z(); }
    debug() << endmsg;
  }

  // Optional 'missing' track states, for start(0) and end(1) points
  std::array<std::optional<LHCb::State>, 2> missing_states;

  // Loop over all radiators
  for ( const auto radiator : m_radiators ) {

    // which radiator
    const auto rad = radiator->radiatorID();
    _ri_verbo << " Considering radiator " << rad << endmsg;

    // skip rad ?
    if ( UNLIKELY( skipByType( track, rad ) ) ) { continue; }

    // choose appropriate z start position for initial track states for this radiator
    const auto zStart = ( Rich::Rich2Gas == rad ? m_nomZstates[2] : m_nomZstates[0] );

    // which RICH
    const auto rich = radiator->rich();

    // Get the track entry state points
    const auto* entrPStateRaw = &( track.closestState( zStart ) );
    if ( UNLIKELY( !entrPStateRaw ) ) {
      ++m_stateFromTrackErr;
      continue;
    }

    // check tolerance
    _ri_verbo << " -> Closest Entry State at z=" << entrPStateRaw->z() << "mm" << endmsg;
    const auto entryTol = zStart - entrPStateRaw->z();
    if ( fabs( entryTol ) > m_zTolerance[rad] ) {
      _ri_verbo << "  -> Entry State : Requested z=" << zStart << " found z=" << entrPStateRaw->z()
                << " failed tolerance check dz=" << m_zTolerance[rad] << endmsg;
      entrPStateRaw = nullptr;
    }

    // Failed to find the state, so try with the state provider....
    if ( UNLIKELY( !entrPStateRaw && m_createMissingStates.value() ) ) {
      ++m_createMissingEntrState;
      const auto sc = m_trStateP.get()->state( missing_states[0].emplace(), track, zStart );
      if ( sc ) {
        entrPStateRaw = &missing_states[0].value();
        _ri_debug << "   -> Found entry state at z=" << zStart << "mm via StateProvider" << endmsg;
      } else {
        _ri_verbo << "   -> Failed to get entry State at z=" << zStart << "mm via StateProvider" << endmsg;
      }
    }
    // if still no state, skip this track
    if ( UNLIKELY( !entrPStateRaw ) ) {
      ++m_failedEntryState;
      continue;
    }

    // check above electron threshold
    if ( richPartProps()->thresholdMomentum( Rich::Electron, rad ) > entrPStateRaw->p() ) {
      _ri_verbo << "  -> Below electron cherenkov threshold -> reject" << endmsg;
      continue;
    }

    // choose appropriate z end position for initial track states for this radiator
    const auto zEnd = ( Rich::Rich2Gas == rad ? m_nomZstates[3] : m_nomZstates[1] );

    // Get the track exit state points
    const auto* exitPStateRaw = &( track.closestState( zEnd ) );
    if ( UNLIKELY( !exitPStateRaw ) ) {
      ++m_stateFromTrackErr;
      continue;
    }

    // check tolerance
    _ri_verbo << " -> Closest Exit State at  z=" << exitPStateRaw->z() << "mm" << endmsg;
    const auto exitTol = zEnd - exitPStateRaw->z();
    if ( fabs( exitTol ) > m_zTolerance[rad] ) {
      _ri_verbo << "  -> Exit State  : Requested z=" << zEnd << " found z=" << exitPStateRaw->z()
                << " failed tolerance check dz=" << m_zTolerance[rad] << endmsg;
      exitPStateRaw = nullptr;
    }

    // Failed to find the state, so try with the state provider....
    if ( UNLIKELY( !exitPStateRaw && m_createMissingStates.value() ) ) {
      ++m_createMissingExitState;
      const auto sc = m_trStateP.get()->state( missing_states[1].emplace(), track, zEnd );
      if ( sc ) {
        exitPStateRaw = &missing_states[1].value();
        _ri_debug << "    -> Found exit state at z=" << zEnd << "mm via StateProvider" << endmsg;
      } else {
        _ri_verbo << "    -> Failed to get exit State at z=" << zEnd << "mm via StateProvider" << endmsg;
      }
    }
    // if still no state, skip this track
    if ( UNLIKELY( !exitPStateRaw ) ) {
      ++m_failedExitState;
      continue;
    }

    // Clone local working copies of entry and exit states
    LHCb::State entrPState( *entrPStateRaw ), exitPState( *exitPStateRaw );
    _ri_verbo << "  Found appropriate initial start/end States" << endmsg //
              << "   EntryPos : " << entrPState.position() << endmsg      //
              << "   EntryDir : " << entrPState.slopes() << endmsg        //
              << "   ExitPos  : " << exitPState.position() << endmsg      //
              << "   ExitDir  : " << exitPState.slopes() << endmsg;

    // Radiator intersections for entry and exit states
    Rich::RadIntersection::Vector rad_ints;

    // order of entry/exit states to try
    const bool entryFirst = entryStateForIntersects( track, rad );
    // get intersects for for state to try
    const auto& rad_int_s = ( entryFirst ? entrPState : exitPState );
    getRadIntersections( rad_int_s.position(), rad_int_s.slopes(), rad, rad_ints );

    // Is momentum low enough to try and finesse the state info to account for curvature ?
    if ( UNLIKELY( ( entrPState.p() < m_maxPstateFinesse[rich] ) && !rad_ints.empty() ) ) {
      // extrapolate to the entry z position found above
      const auto& entr_pnt = rad_ints.front().entryPoint();
      if ( moveState( entrPState, track, entr_pnt.z(), rich ) ) {
        // (re)find radiator entry and exit points
        if ( fabs( entrPState.z() - entr_pnt.z() ) < m_zTolerance[rad] ) {
          getRadIntersections( entrPState.position(), entrPState.slopes(), rad, rad_ints );
        }
      }
    }

    // Did we find any intersections ?
    if ( UNLIKELY( rad_ints.empty() ) ) {
      _ri_verbo << "  Failed to get radiator intersects" << endmsg;
      continue;
    }

    // Move entry state to best position
    bool sc = moveState( entrPState, track, rad_ints.front().entryPoint().z(), rich );

    //---------------------------------------------------------------------------------------------
    // For exit point additional correction for mirror is needed

    // curren best guess for z position of exit point, based on radiator intersections
    const auto initialZ = rad_ints.back().exitPoint().z();

    // move exit state to be on the inside of the mirror
    _ri_verbo << "    --> Moving state first to be inside mirror" << endmsg;
    sc &= moveState( exitPState, track, initialZ - m_mirrShift[rich], rich );

    // get the RICH side
    const auto side = m_rich[rich]->side( exitPState.position() );

    // attempt to intersect with the primary mirror
    Gaudi::XYZPoint intersection;
    // do the intersection
    bool correct = intersectSpherical( exitPState.position(), exitPState.slopes(),     //
                                       m_rich[rich]->nominalCentreOfCurvature( side ), //
                                       m_rich[rich]->sphMirrorRadius(), intersection );
    // are we inside the mirror shell ?
    correct &= ( intersection.z() < initialZ && radiator->geometry()->isInside( intersection ) );

    // finally move the exit state to the final z position
    sc &= moveState( exitPState, track, ( correct ? intersection.z() : initialZ ), rich );

    // Test final status code
    if ( UNLIKELY( !sc ) ) {
      _ri_verbo << "    --> Failed to use state information. Quitting." << endmsg;
      continue;
    }

    //---------------------------------------------------------------------------------------------
    // Correction for beam pipe intersections
    if ( checkBeamPipe( rad ) ) {
      // intersection points
      Gaudi::XYZPoint inter1, inter2;
      // Get intersections with beam pipe using DeRich object
      const auto intType = deBeam( rad )->intersectionPoints( entrPState.position(), //
                                                              exitPState.position(), //
                                                              inter1, inter2 );

      _ri_verbo << "  --> Beam Intersects : " << intType << " : " << inter1 << " " << inter2 << endmsg;

      sc = true;
      if ( intType == DeRichBeamPipe::NoIntersection ) {
        _ri_verbo << "   --> No beam intersections -> No corrections needed" << endmsg;
      } else if ( intType == DeRichBeamPipe::FrontAndBackFace ) {
        _ri_verbo << "   --> Inside beam pipe -> Reject segment" << endmsg;
        continue;
      } else if ( intType == DeRichBeamPipe::FrontFaceAndCone ) {
        // Update entry point to exit point on cone
        _ri_verbo << "   --> Correcting entry point to point on cone" << endmsg;
        sc = moveState( entrPState, track, inter2.z(), rich );
      } else if ( intType == DeRichBeamPipe::BackFaceAndCone ) {
        // Update exit point to entry point on cone
        _ri_verbo << "   --> Correcting exit point to point on cone" << endmsg;
        sc = moveState( exitPState, track, inter1.z(), rich );
      }
      if ( UNLIKELY( !sc ) ) {
        _ri_verbo << "    --> Error fixing radiator entry/exit points for beam-pipe. Quitting." << endmsg;
        continue;
      }
    }
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // Final check that info is reasonable
    //---------------------------------------------------------------------------------------------
    const bool Zcheck     = entrPState.z() > exitPState.z();
    const bool ZdiffCheck = ( exitPState.z() - entrPState.z() ) < m_minStateDiff[rad];
    if ( UNLIKELY( Zcheck || ZdiffCheck ) ) { continue; }
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    // Radiator path length cut
    //---------------------------------------------------------------------------------------------
    if ( UNLIKELY( ( exitPState.position() - entrPState.position() ).Mag2() < m_minRadLengthSq[rad] ) ) {
      _ri_verbo << "    --> Path length too short -> rejecting segment" << endmsg;
      continue;
    }
    //---------------------------------------------------------------------------------------------

    // Create final entry and exit momentum vectors
    auto entryStateMomentum( entrPState.slopes() );
    entryStateMomentum *= entrPState.p() / std::sqrt( entryStateMomentum.Mag2() );
    auto exitStateMomentum( exitPState.slopes() );
    exitStateMomentum *= exitPState.p() / std::sqrt( exitStateMomentum.Mag2() );

    // Update final intersections
    rad_ints.front().setEntryPoint( entrPState.position() );
    rad_ints.front().setEntryMomentum( entryStateMomentum );
    rad_ints.back().setExitPoint( exitPState.position() );
    rad_ints.back().setExitMomentum( exitStateMomentum );

    // Errors for entry and exit states
    const LHCb::RichTrackSegment::StateErrors entryErrs( entrPState );
    const LHCb::RichTrackSegment::StateErrors exitErrs( exitPState );

    // print out final points
    _ri_verbo << "  Found final points :-" << endmsg << "   Entry : Pnt=" << rad_ints.front().entryPoint()
              << " Mom=" << rad_ints.front().entryMomentum()
              << " Ptot=" << std::sqrt( rad_ints.front().entryMomentum().Mag2() ) << endmsg;
    _ri_verbo << "   Exit  : Pnt=" << rad_ints.back().exitPoint() << " Mom=" << rad_ints.back().exitMomentum()
              << " Ptot=" << std::sqrt( rad_ints.back().exitMomentum().Mag2() ) << endmsg;

    // if get here segment will be saved so save relations
    segList.push_back( segments.size() ); // this gives the index for the next entry ...
    segToTkRel.push_back( tkIndex );

    // ================================== NOTE ==========================================
    //
    // From now on we might use move semantics, as we no longer care about the
    // various data objects once they have been used to create a RichTrackSegment object.
    // So must be careful not 'move' something before it is finished with....
    //
    // ==================================================================================

    // For gas radiators transport entry state to mid point to create middle point
    // information for three point RichTrackSegment constructor

    const auto middle = createMiddleInfo( track, rich, entrPState, exitPState );
    if ( LIKELY( middle.status ) ) {
      // Using this information, make radiator segment
      // this version uses 3 states and thus incorporates some concept of track curvature
      segments.emplace_back( std::move( rad_ints ),         //
                             middle.point, middle.momentum, //
                             rad, rich,                     //
                             entryErrs, middle.errors, exitErrs );
    } else {
      // Using this information, make radiator segment
      // this version uses 2 states and thus forces a straight line approximation
      segments.emplace_back( std::move( rad_ints ), rad, rich, entryErrs, exitErrs );
    }

    // Set mean photon energy
    segments.back().setAvPhotonEnergy( m_detParameters.get()->meanPhotonEnergy( rad ) );

  } // end loop over radiators

  // Final printout of states, to see if anything has changed ...
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Finished with Track key=" << track.key() << " history=" << track.history() << " : "
              << track.states().size() << " States at z =";
    for ( const auto* S : track.states() ) { verbose() << " " << S->z(); }
    verbose() << endmsg;
  }
}

//====================================================================================================
// creates middle point info
DetailedTrSegMakerFromTracks::TrackData
DetailedTrSegMakerFromTracks::createMiddleInfo( const LHCb::Track&       track,  //
                                                const Rich::DetectorType rich,   //
                                                LHCb::State&             fState, //
                                                LHCb::State&             lState  //
                                                ) const {

  _ri_verbo << "   --> Creating middle point information" << endmsg;

  TrackData middle;

  // middle point z position
  const auto midZ = 0.5 * ( fState.position().z() + lState.position().z() );

  // move start state to this z
  const auto moveFirst = moveState( fState, track, midZ, rich );

  // move end state to this z
  const auto moveLast = ( UNLIKELY( m_useBothStatesForMiddle.value() && Rich::Rich1 == rich ) //
                              ? moveState( lState, track, midZ, rich )
                              : false );

  if ( UNLIKELY( moveFirst && moveLast ) ) {
    middle.point    = fState.position() + ( lState.position() - fState.position() ) * 0.5;
    middle.momentum = ( fState.slopes() + lState.slopes() ) * 0.5;
    middle.momentum *= ( fState.p() + lState.p() ) / ( 2.0 * std::sqrt( middle.momentum.Mag2() ) );
    middle.errors = LHCb::RichTrackSegment::StateErrors( ( fState.errX2() + lState.errX2() ) * 0.5,   //
                                                         ( fState.errY2() + lState.errY2() ) * 0.5,   //
                                                         ( fState.errTx2() + lState.errTx2() ) * 0.5, //
                                                         ( fState.errTy2() + lState.errTy2() ) * 0.5, //
                                                         ( fState.errP2() + lState.errP2() ) * 0.5 );
  } else if ( moveFirst ) {
    middle.point    = fState.position();
    middle.momentum = fState.slopes();
    middle.momentum *= fState.p() / std::sqrt( middle.momentum.Mag2() );
    middle.errors = LHCb::RichTrackSegment::StateErrors( fState );
  } else if ( moveLast ) {
    middle.point    = lState.position();
    middle.momentum = lState.slopes();
    middle.momentum *= lState.p() / std::sqrt( middle.momentum.Mag2() );
    middle.errors = LHCb::RichTrackSegment::StateErrors( lState );
  }

  middle.status = ( moveFirst || moveLast );

  return middle;
}
//====================================================================================================

//====================================================================================================
bool DetailedTrSegMakerFromTracks::moveState( LHCb::State&             stateToMove, //
                                              const LHCb::Track&       track,       //
                                              const double             z,           //
                                              const Rich::DetectorType rich         //
                                              ) const {

  bool OK = true;

  // Check if requested move is big enough to bother with
  const auto zdiff     = z - stateToMove.z();
  const auto abs_zdiff = fabs( zdiff );
  if ( abs_zdiff > m_minZmove[rich] ) {

    // verbose printout
    _ri_verbo << "    --> Extrapolating state from " << stateToMove.position() << endmsg;

    // If move is still 'small' use in house linear move.
    if ( UNLIKELY( abs_zdiff < m_linZmove[rich] ) ) {

      // use basic fast linear extrapolation.
      // do not bother updating errors...
      stateToMove.setX( stateToMove.x() + ( zdiff * stateToMove.tx() ) );
      stateToMove.setY( stateToMove.y() + ( zdiff * stateToMove.ty() ) );
      stateToMove.setZ( z );

    } else {

      // Use State provider to move the state
      if ( UNLIKELY( m_useStateProvider.value() ) ) {
        // if ( !m_trStateP.get()->state(stateToMove,track,z) )
        if ( UNLIKELY( !m_trStateP.get()->stateFromTrajectory( stateToMove, track, z ) ) ) { OK = false; }
      } else {
        // try with the extrapolator
        OK = m_trExt.get()->propagate( stateToMove, z ).isSuccess();
      }
    }

    // verbose printout
    _ri_verbo << "                            to   " << stateToMove.position() << endmsg;

  } // move > min

  return OK;
}
//====================================================================================================

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetailedTrSegMakerFromTracks )

//=============================================================================
