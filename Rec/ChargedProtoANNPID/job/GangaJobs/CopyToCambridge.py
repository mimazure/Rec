#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getGangaIDs(jobname):

    print "Getting Job IDs matching name", jobname

    from GangaCore.GPI import jobs

    jobids = [j.id for j in jobs.select(name=jobname)]

    return jobids


def moveCalibrationFiles():

    import glob, os, shutil, sys, random

    from GangaCore.GPI import jobs

    mergeJobs = [

        # Upgrade
        {
            "EventTypes": [
                "11102005", "11104124", "11114001", "11124001", "11144201",
                "11296013", "11874091", "11874401", "12103032", "12143001",
                "12163021", "12873441", "13104001", "13112001", "13144011",
                "13154001", "13264021", "13264031", "13512010", "15204010",
                "15336011", "15874041", "16166011", "18112001", "27163002",
                "27165000", "27373001", "42122000", "37103000", "11102404",
                "11114101", "11140400", "11142401", "11144008", "11144103",
                "11160001", "11574001", "12103022", "12165106", "12203224",
                "12245021", "12103213", "12875401", "13102202", "13104012",
                "13134061", "13774004", "14177001", "14195002", "21163000",
                "27165100", "27165101", "27175002", "31113001", "32113001",
                "34112106", "34112407", "40114010", "42112050", "42311000",
                "42321000", "49000054", "11102202", "15102307", "12265042",
                "14103034"
            ],
            "Regex":
            "*-ANNPID-*Sim09c-Up02Reco-Up01*",
            "FirstID":
            1
        }

        # Deuteron
        #{ "EventTypes" : ["15102021","15104040"],
        #  "Regex" : "*-ANNPID-*P8Sim09bT0x6138160FR16Tb03S28NoPF*",
        #  "FirstID" : 1 }
    ]

    gangaDir = "/usera/jonesc/gangadir/workspace/jonesc/LocalXML/"

    outputRoot = "/r02/lhcb/jonesc/ANNPID/ProtoParticlePIDtuples/MC/Sim09c-Up02/Reco-Up01/"

    for mergeJob in mergeJobs:

        for evtT in mergeJob["EventTypes"]:

            outPath = outputRoot + evtT

            if not os.path.exists(outPath): os.makedirs(outPath)

            jobids = getGangaIDs(evtT + mergeJob["Regex"])

            rootfiles = []
            for id in jobids:
                newfiles = glob.glob(gangaDir + str(id) +
                                     '/*/output/ProtoPIDANN.MC.tuples.root')
                print " -> Globbed", len(newfiles), "root files from", jobs(
                    id).name
                for file in newfiles:
                    rootfiles.append(file)
            random.shuffle(rootfiles)

            filelist = evtT + "-list.txt"

            file = open(filelist, "w")

            i = mergeJob["FirstID"]
            for rootfile in rootfiles:
                target = outPath + "/ANNPID." + str(i).zfill(8) + ".root"
                # Does this target already exist ?
                while os.path.exists(target):
                    i = i + 1
                    #print "WARNING :",  target, "already exists"
                    target = outPath + "/ANNPID." + str(i).zfill(8) + ".root"
                print "Moving", rootfile, "to", target
                shutil.move(rootfile, target)
                file.write(target + ":ANNPID/DecayTree\n")
                i = i + 1

            file.close()
