/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddEcalInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddEcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H 1

#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

/** @class FutureChargedProtoParticleAddEcalInfo FutureChargedProtoParticleAddEcalInfo.h
 *
 *  Updates the ECAL information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddEcalInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg {

public:
  /// Standard constructor
  FutureChargedProtoParticleAddEcalInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Load the Calo Ecal tables
  bool getEcalData();

  /// Add Calo Ecal information to the given ProtoParticle
  bool addEcal( LHCb::ProtoParticle* proto ) const;

private:
  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inEcalPath;
  std::string m_electronMatchPath;
  std::string m_clusterMatchPath;
  std::string m_ecalChi2Path;
  std::string m_ecalEPath;
  std::string m_clusterChi2Path;
  std::string m_ecalPIDePath;
  std::string m_ecalPIDmuPath;

  const LHCb::CaloFuture2Track::ITrAccTable*    m_InEcalTable    = nullptr;
  const LHCb::CaloFuture2Track::IHypoTrTable2D* m_elecTrTable    = nullptr;
  const LHCb::CaloFuture2Track::IClusTrTable2D* m_clusTrTable    = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*   m_EcalChi2Table  = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*   m_EcalETable     = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*   m_ClusChi2Table  = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*   m_dlleEcalTable  = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*   m_dllmuEcalTable = nullptr;

  /// CaloElectron tool
  LHCb::Calo::Interfaces::IElectron* m_electron = nullptr;
};

#endif // GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H
