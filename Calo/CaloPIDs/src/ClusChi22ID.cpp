/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct ClusChi22ID final : public CaloChi22ID<TABLEI, TABLEO> {
  static_assert( std::is_base_of<LHCb::Calo2Track::IClusTrTable2D, TABLEI>::value,
                 "TABLEI must inherit from IClusTrTable2D" );

  ClusChi22ID( const std::string& name, ISvcLocator* pSvc ) : CaloChi22ID<TABLEI, TABLEO>( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    updateHandleLocation( *this, "Input", CaloIdLocation( "ClusterMatch", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "ClusChi2", context() ) );
    // @todo it must be in agrement with "Threshold" for PhotonMatchAlg
    _setProperty( "CutOff", "1000" ); //
    // track types:
    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                               LHCb::Track::Types::Downstream ) );
  };
};
// ============================================================================

DECLARE_COMPONENT( ClusChi22ID )
