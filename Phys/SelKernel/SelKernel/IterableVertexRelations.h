/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrZip.h"
#include "SelKernel/VertexRelation.h"

namespace Sel::VertexRelations {
  /** Proxy type for iterating over BestVertexRelations objects. */
  template <typename MergedProxy, typename dType, bool unwrap>
  struct Proxy {
    // TODO these next four lines could/should be macro'd
    BestVertexRelations const* m_rels{nullptr};
    Proxy( BestVertexRelations const* rels ) : m_rels{rels} {}
    auto offset() const { return static_cast<MergedProxy const&>( *this ).offset(); }
    auto size() const { return m_rels->size(); }

    auto bestPV() const { return m_rels->bestPV<dType, unwrap>( this->offset() ); }
  };
} // namespace Sel::VertexRelations

template <>
struct LHCb::Pr::Proxy<BestVertexRelations> {
  template <typename MergedProxy, typename dType, bool unwrap>
  using type = Sel::VertexRelations::Proxy<MergedProxy, dType, unwrap>;
};