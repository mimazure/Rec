{% set used = [] -%}
{% macro section(labels) -%}
{% for mr in select_mrs(merge_requests, labels, used) %}
- {{mr.title}}, !{{mr.iid}} (@{{mr.author.username}}) {{find_tasks(mr)}}  
  {{mr.description|mdindent(2)}}
{% endfor %}
{%- endmacro %}

{{date}} {{project}} {{version}}
===

This version uses Lbcom v30rX, LHCb v50rX, Gaudi v32r2 and LCG_96b with ROOT 6.18.04

This version is released on `master` branch.

Built relative to Rec v30rX, with the following changes:

### New features
{{ section(['new feature']) }}

### Enhancements
{{ section(['enhancement']) }}

### Thread safety
{{ section(['thread-safety']) }}

### Bug fixes
{{ section(['bug fix']) }}

### Code modernisations and cleanups
{{ section(['cleanup', 'modernisation']) }}

### Monitoring changes
{{ section(['monitoring']) }}

### Changes to tests
{{ section(['testing']) }}

### Other
{{ section([[]]) }}
