/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FUTUREGAMMAPI0SEPARATIONTOOL_H
#define FUTUREGAMMAPI0SEPARATIONTOOL_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "GaudiAlg/GaudiTool.h"

// Math
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

// using namespace LHCb;

#include "TMV_MLP_inner.C"
#include "TMV_MLP_middle.C"
#include "TMV_MLP_outer.C"

/** @class FutureGammaPi0SeparationTool FutureGammaPi0SeparationTool.h
 *
 *
 *  @author Miriam Calvo Gomez
 *  @date   2019-03-28
 */
namespace LHCb::Calo {
  class GammaPi0Separation : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;
    StatusCode finalize() override;

    std::optional<double> isPhoton( const LHCb::CaloHypo& hypo ) override;
    std::optional<double> isPhoton( LHCb::span<const double> v ) override {
      return photonDiscriminant( int( v[0] ), v[1], v[2], v[3], v[4], v[5], v[6] );
    }
    const std::map<Enum::DataType, double>& inputDataMap() override { return m_data; }

  private:
    bool ClusterVariables( const LHCb::CaloHypo& hypo, double& fr2, double& fasym, double& fkappa, double& fr2r4,
                           double& etot, double& Eseed, double& E2, int& area );
    std::optional<double> photonDiscriminant( int area, double r2, double r2r4, double asym, double kappa, double Eseed,
                                              double E2 ) const;

    Gaudi::Property<float> m_minPt{this, "MinPt", 2000.};

    std::unique_ptr<IClassifierReader> m_reader0;
    std::unique_ptr<IClassifierReader> m_reader1;
    std::unique_ptr<IClassifierReader> m_reader2;

    const DeCalorimeter* m_ecal = nullptr;

    std::map<Enum::DataType, double> m_data;
  };
} // namespace LHCb::Calo
#endif // FUTUREGAMMAPI0SEPARATIONTOOL_H
