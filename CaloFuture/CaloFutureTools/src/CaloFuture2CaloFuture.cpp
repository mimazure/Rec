/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "Kernel/CaloCellCode.h"
// ============================================================================
// CaloFutureUtils
// ============================================================================
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFuture2CaloFuture.h"
// ============================================================================
/** @file
 *  Implementation file for class : CaloFuture2CaloFuture
 *  @date 2007-05-29
 *  @author Olivier Deschamps
 */
// ============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFuture2CaloFuture )
// ============================================================================

//=============================================================================
StatusCode CaloFuture2CaloFuture::initialize() {
  StatusCode sc = base_class::initialize();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Initialize CaloFuture2CaloFuture tool " << endmsg;

  // CaloDigit locations
  m_loc[CaloCellCode::CaloIndex::EcalCalo] = LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" );
  m_loc[CaloCellCode::CaloIndex::HcalCalo] = LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" );
  m_loc[CaloCellCode::CaloIndex::PrsCalo]  = LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Prs" );
  m_loc[CaloCellCode::CaloIndex::SpdCalo]  = LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Spd" );
  // DeCalorimeter* pointers
  int mask                                 = m_detMask.value();
  m_det[CaloCellCode::CaloIndex::EcalCalo] = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  m_det[CaloCellCode::CaloIndex::HcalCalo] = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  m_det[CaloCellCode::CaloIndex::PrsCalo] =
      ( ( mask & 2 ) == 0 ) ? nullptr : getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Prs );
  m_det[CaloCellCode::CaloIndex::SpdCalo] =
      ( ( mask & 1 ) == 0 ) ? nullptr : getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Spd );
  // CellSize reference (outer section)  Warning : factor 2 for Hcal
  m_refSize[CaloCellCode::CaloIndex::HcalCalo] =
      m_det[CaloCellCode::CaloIndex::HcalCalo]->cellParams().front().size() / 2.;
  m_refSize[CaloCellCode::CaloIndex::EcalCalo] = m_det[CaloCellCode::CaloIndex::EcalCalo]->cellParams().front().size();
  if ( m_det[CaloCellCode::CaloIndex::PrsCalo] )
    m_refSize[CaloCellCode::CaloIndex::PrsCalo] = m_det[CaloCellCode::CaloIndex::PrsCalo]->cellParams().front().size();
  if ( m_det[CaloCellCode::CaloIndex::SpdCalo] )
    m_refSize[CaloCellCode::CaloIndex::SpdCalo] = m_det[CaloCellCode::CaloIndex::SpdCalo]->cellParams().front().size();
  // CaloPlanes
  m_plane[CaloCellCode::CaloIndex::HcalCalo] = m_det[CaloCellCode::CaloIndex::HcalCalo]->plane( CaloPlane::ShowerMax );
  m_plane[CaloCellCode::CaloIndex::EcalCalo] = m_det[CaloCellCode::CaloIndex::EcalCalo]->plane( CaloPlane::ShowerMax );
  if ( m_det[CaloCellCode::CaloIndex::PrsCalo] )
    m_plane[CaloCellCode::CaloIndex::PrsCalo] = m_det[CaloCellCode::CaloIndex::PrsCalo]->plane( CaloPlane::ShowerMax );
  if ( m_det[CaloCellCode::CaloIndex::SpdCalo] )
    m_plane[CaloCellCode::CaloIndex::SpdCalo] = m_det[CaloCellCode::CaloIndex::SpdCalo]->plane( CaloPlane::ShowerMax );

  reset();

  if ( ( m_fromCalo == CaloCellCode::CaloIndex::HcalCalo || m_fromCalo == CaloCellCode::CaloIndex::EcalCalo ||
         m_fromCalo == CaloCellCode::CaloIndex::PrsCalo || m_fromCalo == CaloCellCode::CaloIndex::SpdCalo ) &&
       ( m_toCalo == CaloCellCode::CaloIndex::HcalCalo || m_toCalo == CaloCellCode::CaloIndex::EcalCalo ||
         m_toCalo == CaloCellCode::CaloIndex::PrsCalo || m_toCalo == CaloCellCode::CaloIndex::SpdCalo ) )
    setCalos( m_fromCalo, m_toCalo );
  //

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    if ( !m_det[CaloCellCode::CaloIndex::PrsCalo] &&
         ( m_fromCalo == CaloCellCode::CaloIndex::PrsCalo || m_toCalo == CaloCellCode::CaloIndex::PrsCalo ) )
      debug() << "Try to access non-existing Prs DetectorElement" << endmsg;
    if ( !m_det[CaloCellCode::CaloIndex::SpdCalo] &&
         ( m_fromCalo == CaloCellCode::CaloIndex::SpdCalo || m_toCalo == CaloCellCode::CaloIndex::SpdCalo ) )
      debug() << "Try to access non-existing Spd DetectorElement" << endmsg;
  }
  return sc;
}

void CaloFuture2CaloFuture::reset() const {
  m_digits.clear();
  m_cells.clear();
  m_energy = 0;
  m_count  = 0;
}

void CaloFuture2CaloFuture::setCalos( CaloCellCode::CaloIndex fromCaloFuture,
                                      CaloCellCode::CaloIndex toCaloFuture ) const {

  auto* self = const_cast<CaloFuture2CaloFuture*>( this );

  m_ok = true;
  if ( !self->m_det[CaloCellCode::CaloIndex::PrsCalo] &&
       ( fromCaloFuture == CaloCellCode::CaloIndex::PrsCalo || toCaloFuture == CaloCellCode::CaloIndex::PrsCalo ) ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Try to access non-existing Prs DetectorElement" << endmsg;
    m_ok = false;
    return;
  }

  if ( !self->m_det[CaloCellCode::CaloIndex::SpdCalo] &&
       ( fromCaloFuture == CaloCellCode::CaloIndex::SpdCalo || toCaloFuture == CaloCellCode::CaloIndex::SpdCalo ) ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Try to access non-existing Spd DetectorElement" << endmsg;
    m_ok = false;
    return;
  }

  self->m_fromCalo = fromCaloFuture;
  self->m_toCalo   = toCaloFuture;
  self->m_fromDet  = self->m_det[m_fromCalo];
  self->m_fromSize = self->m_refSize[m_fromCalo];
  self->m_toDet    = self->m_det[m_toCalo];
  self->m_toPlane  = self->m_plane[m_toCalo];
  self->m_toSize   = self->m_refSize[m_toCalo];
  self->m_toLoc    = self->m_loc[m_toCalo];
}
//=======================================================================================================
const std::vector<LHCb::CaloCellID>& CaloFuture2CaloFuture::cellIDs( const LHCb::CaloCluster& fromCluster,
                                                                     CaloCellCode::CaloIndex  toCaloFuture ) const {

  reset();
  LHCb::CaloCellID seedID         = fromCluster.seed();
  auto             fromCaloFuture = seedID.calo();
  if ( toCaloFuture != m_toCalo || fromCaloFuture != m_fromCalo )
    const_cast<CaloFuture2CaloFuture*>( this )->setCalos( fromCaloFuture, toCaloFuture );
  if ( !m_ok ) return m_cells;

  for ( const auto& ent : fromCluster.entries() ) {
    const LHCb::CaloDigit* digit = ent.digit();
    if ( digit ) cellIDs( digit->cellID(), toCaloFuture, false );
  }
  return m_cells;
}
//=======================================================================================================
const std::vector<LHCb::CaloCellID>& CaloFuture2CaloFuture::addCell( const LHCb::CaloCellID& id,
                                                                     CaloCellCode::CaloIndex toCaloFuture ) const {

  if ( !m_ok ) return m_cells;
  // consistency check
  if ( id.calo() != m_toCalo || toCaloFuture != m_toCalo ) {
    Warning( "CellID is not consistent with CaloFuture setting" ).ignore();
    return m_cells;
  }
  // check duplicate
  if ( m_cells.end() != std::find( m_cells.begin(), m_cells.end(), id ) ) { return m_cells; }

  // add the cells
  m_cells.push_back( id );

  // added by VB.
  if ( !m_digs ) {
    Error( "Digits* points to NULL" );
    return m_cells;
  }

  LHCb::CaloDigit* digit = m_digs->object( id );
  if ( digit ) {
    m_digits.push_back( digit );
    m_energy += digit->e();
    m_count++;
  }
  return m_cells;
}

//=======================================================================================================
const std::vector<LHCb::CaloCellID>& CaloFuture2CaloFuture::cellIDs( const LHCb::CaloCellID& fromId,
                                                                     CaloCellCode::CaloIndex toCaloFuture,
                                                                     bool                    init ) const {

  if ( init ) reset();
  auto fromCaloFuture = fromId.calo();
  if ( toCaloFuture != m_toCalo || fromCaloFuture != m_fromCalo ) setCalos( fromCaloFuture, toCaloFuture );
  if ( !m_ok ) return m_cells;

  LHCb::CaloCellID toId = fromId;
  // ---- Assume ideal geometry : trivial mapping for detectors having the same granularity (Prs/Spd/Ecal)
  if ( ( m_geo &&
         ( m_fromCalo == CaloCellCode::CaloIndex::EcalCalo || m_fromCalo == CaloCellCode::CaloIndex::PrsCalo ||
           m_fromCalo == CaloCellCode::CaloIndex::SpdCalo ) &&
         ( m_toCalo == CaloCellCode::CaloIndex::EcalCalo || m_toCalo == CaloCellCode::CaloIndex::PrsCalo ||
           m_toCalo == CaloCellCode::CaloIndex::SpdCalo ) ) ||
       m_fromCalo.value() == m_toCalo.value() ) {
    toId.setCalo( m_toCalo.value() );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Add cell from trivial mapping" << endmsg;
    return addCell( toId, m_toCalo );
  }

  // ---- Else use the actual geometry to connet detectors
  double          scale  = 1.;
  Gaudi::XYZPoint center = m_fromDet->cellCenter( fromId );
  if ( m_fromCalo.value() != m_toCalo.value() ) {
    // z-scaling
    scale  = m_toSize / m_fromSize;
    center = m_toPlane.ProjectOntoPlane( m_fromDet->cellCenter( fromId ) * scale );
    // connect
    toId = m_toDet->Cell( center );
  }
  double fromSize = m_fromDet->cellSize( fromId ) * scale;
  // cell-center is outside 'toCaloFuture' - check corners
  if ( LHCb::CaloCellID() == toId ) {
    for ( int i = 0; i != 2; ++i ) {
      for ( int j = 0; j != 2; ++j ) {
        double           x        = m_fromDet->cellCenter( fromId ).X() + ( i * 2 - 1 ) * fromSize;
        double           y        = m_fromDet->cellCenter( fromId ).Y() + ( j * 2 - 1 ) * fromSize;
        LHCb::CaloCellID cornerId = m_toDet->Cell( {x, y, center.Z()} );
        if ( LHCb::CaloCellID() == cornerId ) continue;
        toId = cornerId;
      }
    }
  }
  if ( LHCb::CaloCellID() == toId ) return m_cells;
  int    pad = 1;
  double x0  = center.X();
  double y0  = center.Y();
  if ( m_fromDet != m_toDet ) {
    double toSize = m_toDet->cellSize( toId );
    pad           = (int)floor( fromSize / toSize + 0.25 ); // warning int precision
    if ( pad < 1 ) pad = 1;
    x0 = center.X() - ( pad - 1 ) * fromSize / 2. / pad;
    y0 = center.Y() - ( pad - 1 ) * fromSize / 2. / pad;
  }
  for ( int i = 0; i != pad; ++i ) {
    for ( int j = 0; j != pad; ++j ) {
      double          x = x0 + i * fromSize / pad;
      double          y = y0 + j * fromSize / pad;
      Gaudi::XYZPoint pos( x, y, center.Z() );
      if ( m_fromDet != m_toDet ) toId = m_toDet->Cell( pos );
      if ( LHCb::CaloCellID() == toId ) continue;
      addCell( toId, m_toCalo );
    }
  }
  return m_cells;
}

// Digits
const std::vector<LHCb::CaloDigit*>& CaloFuture2CaloFuture::digits( const LHCb::CaloCellID& fromId,
                                                                    CaloCellCode::CaloIndex toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromId, toCaloFuture );
  return m_digits;
}

const std::vector<LHCb::CaloDigit*>& CaloFuture2CaloFuture::digits( const LHCb::CaloCluster& fromCluster,
                                                                    CaloCellCode::CaloIndex  toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromCluster, toCaloFuture );
  return m_digits;
}

// Energy
double CaloFuture2CaloFuture::energy( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromId, toCaloFuture );
  return m_energy;
}

int CaloFuture2CaloFuture::multiplicity( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromId, toCaloFuture );
  return m_count;
}

double CaloFuture2CaloFuture::energy( const LHCb::CaloCluster& fromCluster,
                                      CaloCellCode::CaloIndex  toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromCluster, toCaloFuture );
  return m_energy;
}

int CaloFuture2CaloFuture::multiplicity( const LHCb::CaloCluster& fromCluster,
                                         CaloCellCode::CaloIndex  toCaloFuture ) const {
  const_cast<CaloFuture2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );
  cellIDs( fromCluster, toCaloFuture );
  return m_count;
}

// ============================================================================
// Additional method : isLocalMax
bool CaloFuture2CaloFuture::isLocalMax( const LHCb::CaloDigit& digit ) const {
  const LHCb::CaloCellID& id   = digit.cellID();
  auto                    calo = id.calo();
  DeCalorimeter*          det  = const_cast<CaloFuture2CaloFuture*>( this )->m_det[calo];
  const LHCb::CaloDigits* digits =
      getIfExists<LHCb::CaloDigits>( const_cast<CaloFuture2CaloFuture*>( this )->m_loc[calo] );
  //
  return LHCb::CaloFutureDataFunctor::isLocalMax( &digit, det, digits );
}
