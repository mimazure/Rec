/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURE2CALOFUTURE_H
#define CALOFUTURE2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloFuture2CaloFuture.h" // Interface
/** @class CaloFuture2CaloFuture CaloFuture2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-05-29
 */

class CaloFuture2CaloFuture : public extends<GaudiTool, LHCb::Calo::Interfaces::ICalo2Calo> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;
  // setting
  void setCalos( CaloCellCode::CaloIndex fromCaloFuture, CaloCellCode::CaloIndex toCalo ) const override;
  // CaloCellIDs
  const std::vector<LHCb::CaloCellID>& cellIDs( const LHCb::CaloCluster& fromCluster,
                                                CaloCellCode::CaloIndex  toCalo ) const override;
  const std::vector<LHCb::CaloCellID>& cellIDs( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCalo,
                                                bool init = true ) const override;
  const std::vector<LHCb::CaloCellID>& cellIDs() const override { return m_cells; };
  // Digits
  const std::vector<LHCb::CaloDigit*>& digits( const LHCb::CaloCellID& fromId,
                                               CaloCellCode::CaloIndex toCalo ) const override;
  const std::vector<LHCb::CaloDigit*>& digits( const LHCb::CaloCluster& fromCluster,
                                               CaloCellCode::CaloIndex  toCalo ) const override;
  const std::vector<LHCb::CaloDigit*>& digits() const override { return m_digits; };
  // Energy
  double energy( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCalo ) const override;
  double energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  double energy() const override { return m_energy; };
  // multiplicity
  int multiplicity( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCalo ) const override;
  int multiplicity( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const override;
  int multiplicity() const override { return m_count; };
  // Additional
  bool isLocalMax( const LHCb::CaloDigit& digit ) const override;

protected:
  //
  void                                 reset() const;
  const std::vector<LHCb::CaloCellID>& addCell( const LHCb::CaloCellID& id, CaloCellCode::CaloIndex toCalo ) const;
  // CaloFuture Maps
  std::map<CaloCellCode::CaloIndex, DeCalorimeter*> m_det;
  std::map<CaloCellCode::CaloIndex, std::string>    m_loc;
  std::map<CaloCellCode::CaloIndex, double>         m_refSize;
  std::map<CaloCellCode::CaloIndex, Gaudi::Plane3D> m_plane;
  //
  Gaudi::Property<CaloCellCode::CaloIndex> m_fromCalo{this, "FromCalo", static_cast<CaloCellCode::CaloIndex>( -1 )};
  Gaudi::Property<CaloCellCode::CaloIndex> m_toCalo{this, "ToCalo", static_cast<CaloCellCode::CaloIndex>( -1 )};
  mutable std::vector<LHCb::CaloCellID>    m_cells;
  mutable std::vector<LHCb::CaloDigit*>    m_digits;
  mutable double                           m_energy   = 0.;
  mutable int                              m_count    = 0;
  DeCalorimeter*                           m_fromDet  = nullptr;
  DeCalorimeter*                           m_toDet    = nullptr;
  double                                   m_fromSize = 0.;
  double                                   m_toSize   = 0.;
  std::string                              m_toLoc;
  Gaudi::Plane3D                           m_toPlane;
  LHCb::CaloDigits*                        m_digs = nullptr;
  mutable bool                             m_ok   = true;

private:
  Gaudi::Property<bool> m_geo{this, "IdealGeometry", true};
  Gaudi::Property<int>  m_detMask{this, "DetectorMask", 0x8 + 0x4};
};
#endif // CALOFUTURE2CALOFUTURE_H
