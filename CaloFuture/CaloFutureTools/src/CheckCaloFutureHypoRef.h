/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHECKCALOFUTUREHYPOREF_H
#define CHECKCALOFUTUREHYPOREF_H 1

// Include files
// from Gaudi
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class CheckCaloFutureHypoRef CheckCaloFutureHypoRef.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2012-05-14
 */
class CheckCaloFutureHypoRef : public GaudiAlgorithm {
public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_inputs{
      this,
      "CaloHypos",
      {LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Photons" ),
       LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Electrons" ),
       LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "MergedPi0s" )}};
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CHECKCALOFUTUREHYPOREF_H
