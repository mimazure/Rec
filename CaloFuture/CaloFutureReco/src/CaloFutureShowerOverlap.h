/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURESHOWEROVERLAP_H
#define CALOFUTURESHOWEROVERLAP_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "FutureSubClusterSelectorTool.h"
#include "GaudiAlg/Transformer.h"
#include "ICaloFutureShowerOverlapTool.h"

/** @class CaloFutureShowerOverlap CaloFutureShowerOverlap.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */
class CaloFutureShowerOverlap
    : public Gaudi::Functional::Transformer<LHCb::CaloCluster::Container( const LHCb::CaloCluster::Container&,
                                                                          const DeCalorimeter& ),
                                            LHCb::DetDesc::usesConditions<DeCalorimeter>>

{

public:
  /// Standard constructor
  CaloFutureShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::CaloCluster::Container operator()( const LHCb::CaloCluster::Container&,
                                           const DeCalorimeter& ) const override; ///< Algorithm
                                                                                  ///< execution

private:
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_cluster_tagging_failed{
      this, "Cluster tagging failed - keep the initial 3x3 tagging"};

  Gaudi::Property<int>   m_dMin{this, "DistanceThreshold", 4};
  Gaudi::Property<float> m_etMin{this, "MinEtThreshold", 50., "( ET1 > x && ET2 > x)"};
  Gaudi::Property<float> m_etMin2{this, "MaxEtThreshold", 150., "( ET2 > y || ET2 > y)"};
  Gaudi::Property<int>   m_iter{this, "Iterations", 5};

  // following properties are inherited by the selector tool when defined :
  Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags"};

  ToolHandle<ICaloFutureShowerOverlapTool> m_oTool{this, "OverlapTool",
                                                   "CaloFutureShowerOverlapTool/PhotonShowerOverlap"};
  ToolHandle<FutureSubClusterSelectorTool> m_tagger{this, "SubClusterSelector",
                                                    "FutureSubClusterSelectorTool/EcalClusterTag"};
};

#endif // CALOFUTURESHOWEROVERLAP_H
