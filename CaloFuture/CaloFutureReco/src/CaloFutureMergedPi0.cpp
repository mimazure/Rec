/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// ============================================================================
#include "CaloFutureMergedPi0.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypo.h"
#include "Event/CellID.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/CaloCellID.h"
#include <algorithm>
#include <cmath>
#include <numeric>
// ============================================================================
/** @file CaloFutureMergedPi0.cpp
 *
 *  Implementation file for class : CaloFutureMergedPi0
 *
 *  @author Olivier Deschamps
 *  @date 05/05/2014
 *
 *  New implementation of CaloFutureMergedPi0 algorithm
 *
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureMergedPi0 )

namespace {
  bool isNeighbor( LHCb::CaloCellID id0, LHCb::CaloCellID id1 ) {
    if ( id0 == LHCb::CaloCellID() || id1 == LHCb::CaloCellID() ) return false;
    if ( abs( int( id0.row() ) - int( id1.row() ) ) > 1 ) return false;
    if ( abs( int( id0.col() ) - int( id1.col() ) ) > 1 ) return false;
    return true;
  }
} // namespace

CaloFutureMergedPi0::CaloFutureMergedPi0( const std::string& name, ISvcLocator* svcloc )
    : MultiTransformer(
          name, svcloc,
          // Input
          {KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "Ecal" )},
           KeyValue{"Detector", DeCalorimeterLocation::Ecal}},
          // Outputs
          {KeyValue{"SplitClusters", LHCb::CaloFutureAlgUtils::CaloFutureSplitClusterLocation()},
           KeyValue{"MergedPi0s", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "MergedPi0s" )},
           KeyValue{"SplitPhotons", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "SplitPhotons" )}} ) {}

// ============================================================================

StatusCode CaloFutureMergedPi0::initialize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialise" << endmsg;
  StatusCode sc = MultiTransformer::initialize();
  if ( sc.isFailure() ) return sc;
  //==== set the tool handle arrays -- FIXME: see gaudi/Gaudi!960
  std::copy( m_photonTools.begin(), m_photonTools.end(), std::back_inserter( m_gTools ) );
  std::copy( m_pi0Tools.begin(), m_pi0Tools.end(), std::back_inserter( m_pTools ) );
  return sc;
}

StatusCode CaloFutureMergedPi0::finalize() {
  m_pTools.clear();
  m_gTools.clear();

  return MultiTransformer::finalize();
}

std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos> CaloFutureMergedPi0::
                                                                 operator()( const LHCb::CaloClusters& clusters, const DeCalorimeter& det ) const {

  auto output                        = std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos>{};
  auto& [splitClusters, pi0s, phots] = output;

  // - setup the estimator of cluster transverse energy
  const auto eT = LHCb::CaloDataFunctor::EnergyTransverse{&det};

  // define entry status
  constexpr LHCb::CaloDigitStatus::Status used = LHCb::CaloDigitStatus::UseForEnergy |
                                                 LHCb::CaloDigitStatus::UseForPosition |
                                                 LHCb::CaloDigitStatus::UseForCovariance;
  constexpr LHCb::CaloDigitStatus::Status seed =
      LHCb::CaloDigitStatus::SeedCell | LHCb::CaloDigitStatus::LocalMaximum | used;

  // book counter buffers
  auto buf_cntNo2ndSeed             = m_cntNo2ndSeed.buffer();
  auto buf_cntFailsToTagECluster1   = m_cntFailsToTagECluster1.buffer();
  auto buf_cntFailsToTagECluster2   = m_cntFailsToTagECluster2.buffer();
  auto buf_cntFailsToSetCovariance1 = m_cntFailsToSetCovariance1.buffer();
  auto buf_cntFailsToSetCovariance2 = m_cntFailsToSetCovariance2.buffer();
  auto buf_cntFailsToSetSpread1     = m_cntFailsToSetSpread1.buffer();
  auto buf_cntFailsToSetSpread2     = m_cntFailsToSetSpread2.buffer();

  // ============ loop over all clusters ==============
  for ( const auto& cluster : clusters ) {
    if ( !cluster ) continue;
    if ( 0 < m_etCut && m_etCut > eT( cluster ) ) continue;

    // -- remove small clusters :
    if ( 1 >= cluster->entries().size() ) continue;

    // -- locate cluster Seed
    const LHCb::CaloCluster::Digits::iterator iSeed = LHCb::CaloDataFunctor::clusterLocateDigit(
        cluster->entries().begin(), cluster->entries().end(), LHCb::CaloDigitStatus::SeedCell );

    LHCb::CaloDigit* dig1 = iSeed->digit();
    if ( !dig1 ) continue;
    LHCb::CaloCellID seed1 = dig1->cellID();

    double seede = dig1->e();

    // -- locate seed2
    double                 sube = 0.; // 2nd seed should must have a positive energy !
    const LHCb::CaloDigit* dig2 = nullptr;
    for ( const auto& it : cluster->entries() ) {
      const LHCb::CaloDigit* dig = it.digit();
      if ( !dig ) { continue; }

      LHCb::CaloCellID seed = dig->cellID();
      if ( seed == seed1 ) continue;

      double ecel = dig->e() * it.fraction();
      if ( ecel > sube && ecel < seede && isNeighbor( seed1, seed ) ) {
        sube = ecel;
        dig2 = dig;
      }
    }

    if ( !dig2 ) {
      buf_cntNo2ndSeed += 1;
      continue;
    }

    LHCb::CaloCellID seed2 = dig2->cellID();

    // -- create and fill sub-cluster
    auto cl1 = std::make_unique<LHCb::CaloCluster>();
    cl1->setSeed( seed1 );
    cl1->setType( LHCb::CaloCluster::Type::Area3x3 );
    auto cl2 = std::make_unique<LHCb::CaloCluster>();
    cl2->setSeed( seed2 );
    cl2->setType( LHCb::CaloCluster::Type::Area3x3 );

    for ( const auto& it2 : cluster->entries() ) {
      const LHCb::CaloDigit* dig = it2.digit();
      if ( !dig ) continue;

      const LHCb::CaloCellID id       = dig->cellID();
      double                 fraction = it2.fraction();

      // -- tag 3x3 area for energy and position
      if ( isNeighbor( seed1, id ) ) {
        LHCb::CaloDigitStatus::Status status = ( seed1 == id ) ? seed : used;
        // set initial weights
        double weight1 = fraction;
        if ( seed2 == id )
          weight1 = 0.;
        else if ( seed1 == id )
          weight1 = fraction;
        else if ( isNeighbor( seed2, id ) )
          weight1 = dig1->e() / ( dig1->e() + dig2->e() ) * fraction;
        cl1->entries().emplace_back( dig, status, weight1 );
      }
      if ( isNeighbor( seed2, id ) ) {
        LHCb::CaloDigitStatus::Status status = ( seed2 == id ) ? seed : used;
        // set initial weights
        double weight2 = fraction;
        if ( seed1 == id )
          weight2 = 0.;
        else if ( seed2 == id )
          weight2 = fraction;
        else if ( isNeighbor( seed1, id ) )
          weight2 = dig2->e() / ( dig1->e() + dig2->e() ) * fraction;
        cl2->entries().emplace_back( dig, status, weight2 );
      }
    }

    // --  apply position tagger (possibly replacing the 3x3 already set)
    // --  needed to apply hypo S/L-corrections with correct parameters internally
    // --  keep the 3x3 energy tag for the time being (to be applied after the overlap subtraction)
    StatusCode sc1, sc2;
    sc1 = m_tagger->tagPosition( *cl1 );
    sc2 = m_tagger->tagPosition( *cl2 );
    if ( sc1.isFailure() || sc2.isFailure() ) ++m_tag_failed;

    // == apply the mergedPi0 tool : subtract shower overlap
    m_oTool->process( *cl1, *cl2, m_iter, ICaloFutureShowerOverlapTool::propagateInitialWeights{true} );

    // skip negative energy "clusters"
    if ( LHCb::CaloMomentum( cl1.get() ).pt() <= m_minET || LHCb::CaloMomentum( cl2.get() ).pt() <= m_minET ) {
      continue;
    }

    // == prepare outputs :

    // == APPLY CLUSTER TOOLS : Energy tagger,  covariance & spread (position tagger already applied):
    // to reduce branching, unconditionally increment all counter buffers
    // cluster energy tag
    buf_cntFailsToTagECluster1 += m_tagger->tagEnergy( *cl1 ).isFailure(); // use implicit conversion of bool to int
    buf_cntFailsToTagECluster2 += m_tagger->tagEnergy( *cl2 ).isFailure();
    // cluster covariance & spread
    buf_cntFailsToSetCovariance1 += ( *m_cov )( *cl1 ).isFailure();
    buf_cntFailsToSetCovariance2 += ( *m_cov )( *cl2 ).isFailure();
    buf_cntFailsToSetSpread1 += ( *m_spread )( *cl1 ).isFailure();
    buf_cntFailsToSetSpread2 += ( *m_spread )( *cl2 ).isFailure();

    // == insert splitClusters into their container
    auto clu1 = cl1.get();
    splitClusters.insert( cl1.release() );
    auto clu2 = cl2.get();
    splitClusters.insert( cl2.release() );

    // ----------------------------------------------------------------------------------------------------
    // DG,20181101: Create CaloHypos always --
    // it seems to be more straightfrward in Gaudi::Functional to fill properly all the outputs rather than
    // try to mimic the old behaviour of optionally filling only some of the output containers.
    // The block below contains the piece of code which could possibly be moved into a separate algorithm,
    // creating CaloHypos from input Clusters and SplitClusters, if separate creation of SplitClusters and
    // CaloHypos (of which the previous version was capable), turns out really necessary.
    // ----------------------------------------------------------------------------------------------------
    {
      // new CaloHypos for splitPhotons
      auto g1 = std::make_unique<LHCb::CaloHypo>();
      g1->setHypothesis( LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 );
      g1->addToClusters( cluster );
      g1->addToClusters( clu1 );
      g1->setPosition( std::make_unique<LHCb::CaloPosition>( clu1->position() ) );

      auto g2 = std::make_unique<LHCb::CaloHypo>();
      g2->setHypothesis( LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 );
      g2->addToClusters( cluster );
      g2->addToClusters( clu2 );
      g2->setPosition( std::make_unique<LHCb::CaloPosition>( clu2->position() ) );

      // new CaloHypo for mergedPi0
      auto pi0 = std::make_unique<LHCb::CaloHypo>();
      pi0->setHypothesis( LHCb::CaloHypo::Hypothesis::Pi0Merged );
      pi0->addToClusters( cluster );
      pi0->addToHypos( g2.get() );
      pi0->addToHypos( g1.get() );

      //--  Apply hypo tools : E/S/L-corrections
      int i = 0;
      for ( const auto& t : m_gTools ) {
        i++;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " apply SplitPhoton tool " << i << "/" << m_gTools.size() << endmsg;
        if ( !t ) continue;
        auto sc = t->process( *g1 );
        if ( sc.isFailure() ) ++m_g1_error;
        sc = t->process( *g2 );
        if ( sc.isFailure() ) ++m_g2_error;
      }

      /// i = 0;
      for ( int i( 0 ), i_max( m_pTools.size() ); i < i_max; i++ ) {
        const auto& t = m_pTools[i];
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " apply MergedPi0 tool " << i << "/" << i_max << endmsg;
        if ( !t ) { continue; }
        auto sc = t->process( *pi0 );
        if ( sc.isFailure() ) ++m_pi0_error;
      }

      // skip negative energy CaloHypos
      if ( LHCb::CaloMomentum( g1.get() ).pt() >= m_minET && LHCb::CaloMomentum( g2.get() ).pt() >= m_minET ) {
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " >> MergedPi0 hypo Mass : " << LHCb::CaloMomentum( pi0.get() ).mass() << endmsg;
        phots.insert( g1.release() );
        phots.insert( g2.release() );
        pi0s.insert( pi0.release() );
      }
    } // --- old !m_createClusterOnly ended here ---
  }

  // ===== process stat counters
  m_cntPi0sSize += pi0s.size();
  m_cntSplitPhotonsSize += phots.size();
  m_cntSplitClustersSize += splitClusters.size();

  return output;
}
